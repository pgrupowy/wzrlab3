#include <stdlib.h>
#include <time.h>

#include "agenty.h"
#include <cmath>


AutoPilot::AutoPilot()
{

}

float getDistance(ObiektRuchomy *ob, Przedmiot item) {

	double distanceX = item.wPol.x - ob->wPol.x;
	double distanceZ = item.wPol.z - ob->wPol.z;

	return sqrt(pow(distanceX, 2.0) + pow(distanceZ, 2.0));

}

float getDistance(Wektor3 *v1, Wektor3 *v2) {
	double distanceX = v1->x - v2->x;
	double distanceY = v1->y - v2->y;
	double distanceZ = v1->z - v2->z;

	return sqrt(pow(distanceX, 2.0) + pow(distanceY, 2.0) + pow(distanceZ, 2.0));
}

float angle(Wektor3 Wa, Wektor3 Wb)
{
	float pii = 3.14159265358979;
	Wektor3 ilo = Wa.znorm2D() * Wb.znorm2D();  // iloczyn wektorowy wektorow o jednostkowej dlugosci
	float sin_kata = ilo.z;        // problem w tym, ze sin(alfa) == sin(pi-alfa)   
	if (sin_kata == 0)
	{
		if (Wa.znorm2D() == Wb.znorm2D()) return 0;
		if (Wa.znorm2D() == -Wb.znorm2D()) return pii;
	}
	// dlatego trzeba  jeszcze sprawdzic czy to kat rozwarty
	Wektor3 wso_n = Wa.znorm2D() + Wb.znorm2D();
	Wektor3 ilo1 = wso_n.znorm2D() * Wb.znorm2D();
	bool kat_rozwarty = (ilo1.dlugosc() > sqrt(2.0) / 2);

	float kat = asin(fabs(sin_kata));
	if (kat_rozwarty) kat = pii - kat;
	if (sin_kata < 0) kat = 2 * pii - kat;

	return kat;
}

void AutoPilot::Sterowanie(ObiektRuchomy *ob,ObiektRuchomy *CudzeObiekty[],long iLiczbaCudzychOb)
{
  // TUTAJ NALE�Y UMIE�CI� ALGORYTM AUTONOMICZNEGO STEROWANIA POJAZDEM
  // .................................................................
  // .................................................................

  Teren *_teren = ob->teren;        

  // wektory lokalnego uk�adu wsp�rz�dnych dla pojazdu:
	Wektor3 w_przod = ob->qOrient.obroc_wektor(Wektor3(1, 0, 0));
	Wektor3 w_gore = ob->qOrient.obroc_wektor(Wektor3(0, 1, 0));
	Wektor3 w_prawo = ob->qOrient.obroc_wektor(Wektor3(0, 0, 1));

	ob->ham = false;
	float speed = sqrt(pow(ob->wV.x, 2.0) + pow(ob->wV.y, 2.0) + pow(ob->wV.z, 2.0));

	float wys_poj = _teren->Wysokosc(ob->wPol.x, ob->wPol.z);  // wysoko�� gleby pod pojazdem
	float V = ob->wV.dlugosc();                                // pr�dko�� pojazdu w m/s 

	double calculatedZ = ob->a*ob->teren->p[ob->closestObjectIndex].wPol.x + ob->b;
	double actualZ = ob->teren->p[ob->closestObjectIndex].wPol.z;

	double toClosestX = ob->teren->p[ob->closestObjectIndex].wPol.x - ob->wPol.x;
	double toClosestZ = ob->teren->p[ob->closestObjectIndex].wPol.z - ob->wPol.z;
	Wektor3 closestVector = Wektor3(toClosestX, toClosestZ, 0);
	Wektor3 velocityVector = Wektor3(ob->wV.x, ob->wV.z, 0);
	float arc = (angle(closestVector, velocityVector)*180.0) / PI;

	if (arc > 354.0 || arc < 6.0) {
		ob->alfa = 0;
	} else if(arc > 180.0) { // if mniejsze to w prawo // else w lewo
		ob->alfa = -ob->alfa_max/1.5;
	}
	else {
		ob->alfa = ob->alfa_max/1.5;
	}
	
	if (ob->closestObjectIndex == -1 || !_teren->p[ob->closestObjectIndex].do_wziecia) {
		// poszukiwanie wybranych przedmiot�w:
		int closesCoinIndex = -1;
		double closestDistance = 0;
		if (ob->ilosc_paliwa < 200.0) {
			for (long i = 0; i < _teren->liczba_przedmiotow; i++) {
				if (_teren->p[i].do_wziecia && _teren->p[i].typ == BECZKA) {
					if (closesCoinIndex == -1) {
						closestDistance = getDistance(ob, _teren->p[i]);
						closesCoinIndex = i;
					}
					else {
						double distance = getDistance(ob, _teren->p[i]);
						if (distance < closestDistance) {
							closestDistance = distance;
							closesCoinIndex = i;
						}
					}
				}
			}
		}
		else { 
			for (long i = 0; i < _teren->liczba_przedmiotow; i++) {
				if (_teren->p[i].do_wziecia
					&& (_teren->p[i].typ == MONETA)
					&& _teren->p[i].wartosc < 2000) {
					if (closesCoinIndex == -1) {
						closestDistance = getDistance(ob, _teren->p[i]);
						closesCoinIndex = i;
					}
					else {
						double distance = getDistance(ob, _teren->p[i]);
						if (_teren->p[i].wartosc > _teren->p[closesCoinIndex].wartosc) {
							closestDistance = distance;
							closesCoinIndex = i;
						}
						else if ((_teren->p[i].wartosc == _teren->p[closesCoinIndex].wartosc) && (distance < closestDistance)) {
							closestDistance = distance;
							closesCoinIndex = i;
						}
					}
				}
			}
		}

		ob->closestObjectIndex = closesCoinIndex;
	}
	double distanceToCoin = getDistance(&ob->wPol, &_teren->p[ob->closestObjectIndex].wPol);

		if (speed > 20.5) {
			ob->F = 0;
		}
		else {
			if (speed > 10.0) {
				ob->F = ob->F_max*0.5;
			}
			else {
				ob->F = ob->F_max;
			}
		}
}

void AutoPilot::TestSterowania(ObiektRuchomy *_ob,float krok_czasowy, float czas_proby)
{
  bool koniec = false;
  float _czas = 0;               // czas liczony od pocz�tku testu
  //FILE *pl = fopen("test_sterowania.txt","w");
  while(!koniec)
  {
    _ob->Symulacja(krok_czasowy,NULL,0);    
    Sterowanie(_ob);
    _czas += krok_czasowy;
    if (_czas >= czas_proby) koniec = true;
	//fprintf(pl,"czas %f, wPol[%f %f %f], got %d, pal %f, F %f, alfa %f, ham %f\n",_czas,_ob->wPol.x,_ob->wPol.y,_ob->wPol.z,_ob->pieniadze,_ob->ilosc_paliwa,_ob->F,_ob->alfa,_ob->ham);
  }
  //fclose(pl);
}

// losowanie liczby z rozkladu normalnego o zadanej sredniej i wariancji
float Randn(float srednia, float wariancja, long liczba_iter)
{
	//long liczba_iter = 10;  // im wiecej iteracji tym rozklad lepiej przyblizony
	float suma = 0;
	for (long i = 0; i<liczba_iter; i++)
		suma += (float)rand() / RAND_MAX;
	return (suma - (float)liczba_iter / 2)*sqrt(12 * wariancja / liczba_iter) + srednia;
}
