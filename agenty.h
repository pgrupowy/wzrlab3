#include "obiekty.h"

class AutoPilot
{       
private:

public:
  AutoPilot();
  void Sterowanie(ObiektRuchomy *ob,ObiektRuchomy *CudzeObiekty[] = NULL,long iLiczbaCudzychOb = 0);                        // pojedynczy krok sterowania
  void TestSterowania(ObiektRuchomy *_ob,float krok_czasowy, float czas_proby); 
};