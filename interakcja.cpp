﻿/************************************************************
Interakcja:
Wysy³anie, odbiór komunikatów, interakcja z innymi
uczestnikami WZR, sterowanie wirtualnymi obiektami  
*************************************************************/

#include <windows.h>
#include <time.h>
#include <stdio.h>   
#include "interakcja.h"
//#include "obiekty.h"
#include "agenty.h"
#include "grafika.h"
#include "siec.h"


bool czy_umiejetnosci = true;          // czy zró¿nicowanie umiejêtnoœci (dla ka¿dego pojazdu losowane s¹ umiejêtnoœci
// zbierania gotówki i paliwa)
bool sterowanie_autonomiczne = false;  // sterowanie autonomiczne pojazdem


FILE *f = fopen("wzr_log.txt","w");     // plik do zapisu informacji testowych

ObiektRuchomy *pMojObiekt;               // obiekt przypisany do tej aplikacji

Teren teren;
int iLiczbaCudzychOb = 0;
ObiektRuchomy *CudzeObiekty[1000];  // obiekty z innych aplikacji lub inne obiekty niz w³asny obiekt
int IndeksyOb[1000];                // tablica indeksow innych obiektow ulatwiajaca wyszukiwanie

AutoPilot *ap;

float fDt;                          // sredni czas pomiedzy dwoma kolejnymi cyklami symulacji i wyswietlania
long czas_cyklu_WS,licznik_sym;     // zmienne pomocnicze potrzebne do obliczania fDt
float sr_czestosc;                  // srednia czestosc wysylania ramek w [ramkach/s] 
long czas_start = clock();          // czas od poczatku dzialania aplikacji  
long czas_istnienia_grupy = clock();    // czas od pocz¹tku istnienia grupy roboczej (czas od uruchom. pierwszej aplikacji)      

multicast_net *multi_reciv;         // wsk do obiektu zajmujacego sie odbiorem komunikatow
multicast_net *multi_send;          //   -||-  wysylaniem komunikatow

HANDLE threadReciv;                 // uchwyt w¹tku odbioru komunikatów
extern HWND okno;       
int SHIFTwcisniety = 0;            
bool rejestracja_uczestnikow = true;   // rejestracja trwa do momentu wziêcia przedmiotu przez któregokolwiek uczestnika,
// w przeciwnym razie trzeba by przesy³aæ ca³y stan œrodowiska nowicjuszowi

// Parametry widoku:
Wektor3 kierunek_kamery = Wektor3(0,-3,-11);   // kierunek patrzenia
Wektor3 pol_kamery = Wektor3(30,3,0);          // po³o¿enie kamery
Wektor3 pion_kamery = Wektor3(0,1,0);           // kierunek pionu kamery             
bool sledzenie = 1;                             // tryb œledzenia obiektu przez kamerê
float oddalenie = 10.0;                          // oddalenie lub przybli¿enie kamery
float zoom = 1.0;                               // zmiana k¹ta widzenia
float kat_kam_z = 0;                            // obrót kamery góra-dó³
bool sterowanie_myszkowe = 0;                   // sterowanie pojazdem za pomoc¹ myszki
int kursor_x, kursor_y;                         // polo¿enie kursora myszki w chwili w³¹czenia sterowania
char napis1[300], napis2[300];                  // napisy wyœwietlane w trybie graficznym 
long nr_miejsca_przedm = teren.liczba_przedmiotow;  // numer miejsca, w którym mo¿na umieœciæ przedmiot

int opoznienia = 0;                

bool tryb_pokazu_autopilota = 0;                // czy pokazywać test autopilota
float biezacy_czas_pokazu_autopilota = 0;       // czas jaki upłynął od początku testu autopilota
float krok_czasowy_autopilota = 0.01;           // stały krok czasowy wolny od możliwości sprzętowych (zamiast fDt)
float czas_testu_autopilota = 600;              // całkowity czas testu

extern float WyslaniePrzekazu(int ID_adresata, int typ_przekazu, float wartosc_przekazu);

enum typy_ramek {STAN_OBIEKTU, WZIECIE_PRZEDMIOTU, ODNOWIENIE_SIE_PRZEDMIOTU, KOLIZJA, PRZEKAZ, 
PROSBA_O_ZAMKNIECIE, NEGOCJACJE_HANDLOWE};

enum typy_przekazu {GOTOWKA, PALIWO};

struct Ramka
{  
	int iID;
	int typ_ramki;
	long czas_wyslania;    
	int iID_adresata;      // nr ID adresata wiadomoœci (pozostali uczestnicy powinni wiadomoœæ zignorowaæ)

	int nr_przedmiotu;     // nr przedmiotu, który zosta³ wziêty lub odzyskany
	Wektor3 wdV_kolid;     // wektor prêdkoœci wyjœciowej po kolizji (uczestnik o wskazanym adresie powinien 
	// przyj¹æ t¹ prêdkoœæ)  

	int typ_przekazu;        // gotówka, paliwo
	float wartosc_przekazu;  // iloœæ gotówki lub paliwa 
	int nr_druzyny;

	StanObiektu stan;

	long czas_istnienia;        // czas jaki uplyn¹³ od uruchomienia programu
};


//******************************************
// Funkcja obs³ugi w¹tku odbioru komunikatów 
DWORD WINAPI WatekOdbioru(void *ptr)
{
	multicast_net *pmt_net=(multicast_net*)ptr;  // wskaŸnik do obiektu klasy multicast_net
	int rozmiar;                                 // liczba bajtów ramki otrzymanej z sieci
	Ramka ramka;
	StanObiektu stan;

	while(1)
	{
		rozmiar = pmt_net->reciv((char*)&ramka,sizeof(Ramka));   // oczekiwanie na nadejœcie ramki 
		switch (ramka.typ_ramki) 
		{
		case STAN_OBIEKTU:           // podstawowy typ ramki informuj¹cej o stanie obiektu              
			{
				stan = ramka.stan;
				//fprintf(f,"odebrano stan iID = %d, ID dla mojego obiektu = %d\n",stan.iID,pMojObiekt->iID);
				int niewlasny = 1;
				if ((ramka.iID != pMojObiekt->iID))          // jeœli to nie mój w³asny obiekt
				{

					if ((rejestracja_uczestnikow) && (IndeksyOb[ramka.iID] == -1))        // nie ma jeszcze takiego obiektu w tablicy -> trzeba go stworzyæ
					{
						CudzeObiekty[iLiczbaCudzychOb] = new ObiektRuchomy(&teren);   
						IndeksyOb[ramka.iID] = iLiczbaCudzychOb;     // wpis do tablicy indeksowanej numerami ID
						// u³atwia wyszukiwanie, alternatyw¹ mo¿e byæ tabl. rozproszona                                                                                                           
						iLiczbaCudzychOb++;     	
						if (ramka.czas_istnienia > czas_istnienia_grupy) czas_istnienia_grupy = ramka.czas_istnienia;
						CudzeObiekty[IndeksyOb[ramka.iID]]->ZmienStan(stan);   // aktualizacja stanu obiektu obcego 
						// wys³anie nowemu uczestnikowi informacji o wszystkich wziêtych przedmiotach:
						for (long i=0;i<teren.liczba_przedmiotow;i++)
							if ((teren.p[i].do_wziecia == 0)&&(teren.p[i].czy_ja_wzialem))
							{
								Ramka ramka;
								ramka.typ_ramki = WZIECIE_PRZEDMIOTU; 
								ramka.nr_przedmiotu = i;
								ramka.stan = pMojObiekt->Stan(); 
								ramka.iID = pMojObiekt->iID;
								int iRozmiar = multi_send->send_delayed((char*)&ramka,sizeof(Ramka));
							}

					}        
					else if (IndeksyOb[ramka.iID] != -1)
						CudzeObiekty[IndeksyOb[ramka.iID]]->ZmienStan(stan);   // aktualizacja stanu obiektu obcego 	
					else if (rejestracja_uczestnikow == false)              // koniec rejestracji                                        
					{
						Ramka ramka;
						ramka.typ_ramki = PROSBA_O_ZAMKNIECIE;                // ¿¹danie zamkniêcia aplikacji
						ramka.iID_adresata = ramka.iID;
						int iRozmiar = multi_send->send_delayed((char*)&ramka,sizeof(Ramka));
					}
				}
				break;
			}
		case WZIECIE_PRZEDMIOTU:            // ramka informuj¹ca, ¿e ktoœ wzi¹³ przedmiot o podanym numerze
			{
				stan = ramka.stan;
				if ((ramka.nr_przedmiotu < teren.liczba_przedmiotow) && (ramka.iID != pMojObiekt->iID))
				{
					teren.p[ramka.nr_przedmiotu].do_wziecia = 0;
					teren.p[ramka.nr_przedmiotu].czy_ja_wzialem = 0;
					//rejestracja_uczestnikow = 0;
				}
				break;
			}
		case ODNOWIENIE_SIE_PRZEDMIOTU:       // ramka informujaca, ¿e przedmiot wczeœniej wziêty pojawi³ siê znowu w tym samym miejscu
			{
				if (ramka.nr_przedmiotu < teren.liczba_przedmiotow)
					teren.p[ramka.nr_przedmiotu].do_wziecia = 1;
				break;
			}
		case KOLIZJA:                       // ramka informuj¹ca o tym, ¿e obiekt uleg³ kolizji
			{
				if (ramka.iID_adresata == pMojObiekt->iID)  // ID pojazdu, który uczestniczy³ w kolizji zgadza siê z moim ID 
				{
					pMojObiekt->wdV_kolid = ramka.wdV_kolid; // przepisuje poprawkê w³asnej prêdkoœci
					pMojObiekt->iID_kolid = pMojObiekt->iID; // ustawiam nr. kolidujacego jako w³asny na znak, ¿e powinienem poprawiæ prêdkoœæ
				}
				break;
			}
		case PRZEKAZ:                       // ramka informuj¹ca o przelewie pieniê¿nym lub przekazaniu towaru    
			{
				if (ramka.iID_adresata == pMojObiekt->iID)  // ID pojazdu, ktory otrzymal przelew zgadza siê z moim ID 
				{       
					if (ramka.typ_przekazu == GOTOWKA)
						pMojObiekt->pieniadze += ramka.wartosc_przekazu; 					
					else if (ramka.typ_przekazu == PALIWO)
						pMojObiekt->ilosc_paliwa += ramka.wartosc_przekazu;

					// nale¿a³oby jeszcze przelew potwierdziæ (w UDP ramki mog¹ byæ gubione!)
				}
				break;
			}
		case PROSBA_O_ZAMKNIECIE:           // ramka informuj¹ca, ¿e powinieneœ siê zamkn¹æ
			{
				if (ramka.iID_adresata == pMojObiekt->iID)
				{   
					SendMessage(okno,WM_DESTROY,0,100);
				}
				break;
			}
		case NEGOCJACJE_HANDLOWE:
			{
				// ------------------------------------------------------------------------
				// --------------- MIEJSCE #1 NA NEGOCJACJE HANDLOWE  ---------------------
				// (szczegó³y na stronie w instrukcji do zadania)




				// ------------------------------------------------------------------------

				break;
			}

		} // switch po typach ramek
	}  // while(1)
	return 1;
}

// *****************************************************************
// ****    Wszystko co trzeba zrobiæ podczas uruchamiania aplikacji
// ****    poza grafik¹   
void PoczatekInterakcji()
{
	DWORD dwThreadId;

	pMojObiekt = new ObiektRuchomy(&teren);    // tworzenie wlasnego obiektu
	if (czy_umiejetnosci == false)
		pMojObiekt->umiejetn_sadzenia = pMojObiekt->umiejetn_zb_monet = pMojObiekt->umiejetn_zb_paliwa = 1.0;

	ap = new AutoPilot();

	for (long i=0;i<1000;i++)            // inicjacja indeksow obcych obiektow
		IndeksyOb[i] = -1;

	czas_cyklu_WS = clock();             // pomiar aktualnego czasu

	// obiekty sieciowe typu multicast (z podaniem adresu WZR oraz numeru portu)
	multi_reciv = new multicast_net("224.10.12.65",10001);      // obiekt do odbioru ramek sieciowych
	multi_send = new multicast_net("224.10.12.65",10001);       // obiekt do wysy³ania ramek

	if (opoznienia)
	{
		float srednie_opoznienie = 3*(float)rand()/RAND_MAX, wariancja_opoznienia = 2;
		multi_send->PrepareDelay(srednie_opoznienie,wariancja_opoznienia);
	}

	// uruchomienie watku obslugujacego odbior komunikatow
	threadReciv = CreateThread(
		NULL,                        // no security attributes
		0,                           // use default stack size
		WatekOdbioru,                // thread function
		(void *)multi_reciv,               // argument to thread function
		0,                           // use default creation flags
		&dwThreadId);                // returns the thread identifier

}


// *****************************************************************
// ****    Wszystko co trzeba zrobiæ w ka¿dym cyklu dzia³ania 
// ****    aplikacji poza grafik¹ 
void Cykl_WS()
{
	licznik_sym++;  

	// obliczenie œredniego czasu pomiêdzy dwoma kolejnnymi symulacjami po to, by zachowaæ  fizycznych 
	if (licznik_sym % 50 == 0)          // jeœli licznik cykli przekroczy³ pewn¹ wartoœæ, to
	{                                   // nale¿y na nowo obliczyæ œredni czas cyklu fDt
		char text[200];
		long czas_pop = czas_cyklu_WS;
		czas_cyklu_WS = clock();
		float fFps = (50*CLOCKS_PER_SEC)/(float)(czas_cyklu_WS-czas_pop);
		if (fFps!=0) fDt=1.0/fFps; else fDt=1;

		sprintf(napis1," %0.0f_fps, paliwo = %0.2f, gotowka = %d,",fFps,pMojObiekt->ilosc_paliwa,pMojObiekt->pieniadze);
		if (licznik_sym % 500 == 0) sprintf(napis2,"");
	}   

	if (tryb_pokazu_autopilota)
	{
		ap->Sterowanie(pMojObiekt);
		pMojObiekt->Symulacja(krok_czasowy_autopilota, CudzeObiekty, iLiczbaCudzychOb);
		biezacy_czas_pokazu_autopilota += krok_czasowy_autopilota;
		sprintf(napis2, "POKAZ TESTU AUTOPILOTA: CZAS = %f", biezacy_czas_pokazu_autopilota);
		if (biezacy_czas_pokazu_autopilota >= czas_testu_autopilota)
    {
			tryb_pokazu_autopilota = false;
		  MessageBox(okno, "Koniec pokazu testu autopilota", "Czy chcesz zamknac program?", MB_OK);
		  SendMessage(okno, WM_DESTROY, 0, 0);
    }
		
	}
	else
		pMojObiekt->Symulacja(fDt,CudzeObiekty,iLiczbaCudzychOb);                    // symulacja w³asnego obiektu


	if ((pMojObiekt->iID_kolid > -1)&&             // wykryto kolizjê - wysy³am specjaln¹ ramkê, by poinformowaæ o tym drugiego uczestnika
		(pMojObiekt->iID_kolid != pMojObiekt->iID)) // oczywiœcie wtedy, gdy nie chodzi o mój pojazd
	{
		Ramka ramka;
		ramka.typ_ramki = KOLIZJA;
		ramka.iID_adresata = pMojObiekt->iID_kolid;
		ramka.wdV_kolid = pMojObiekt->wdV_kolid;
		ramka.iID = pMojObiekt->iID;
		int iRozmiar = multi_send->send_delayed((char*)&ramka,sizeof(Ramka));

		char text[128];
		sprintf(napis2,"Kolizja_z_obiektem_o_ID = %d",pMojObiekt->iID_kolid);
		//SetWindowText(okno,text);

		pMojObiekt->iID_kolid = -1;
	}

	// wyslanie komunikatu o stanie obiektu przypisanego do aplikacji (pMojObiekt):    
	if (licznik_sym % 1 == 0)      
	{
		Ramka ramka;
		ramka.typ_ramki = STAN_OBIEKTU;
		ramka.stan = pMojObiekt->Stan();         // stan w³asnego obiektu 
		ramka.iID = pMojObiekt->iID;
		ramka.czas_istnienia = clock()-czas_start;
		int iRozmiar = multi_send->send_delayed((char*)&ramka,sizeof(Ramka));
	} 


	// wziêcie przedmiotu -> wysy³anie ramki 
	if (pMojObiekt->nr_wzietego_przedm > -1)
	{
		Ramka ramka;
		ramka.typ_ramki = WZIECIE_PRZEDMIOTU; 
		ramka.nr_przedmiotu = pMojObiekt->nr_wzietego_przedm;
		ramka.stan = pMojObiekt->Stan(); 
		ramka.iID = pMojObiekt->iID;
		int iRozmiar = multi_send->send_delayed((char*)&ramka,sizeof(Ramka));

		sprintf(napis2,"Wziecie_przedmiotu_o_wartosci_ %f",pMojObiekt->wartosc_wzieta);

		pMojObiekt->nr_wzietego_przedm = -1;
		pMojObiekt->wartosc_wzieta = 0;
		//rejestracja_uczestnikow = 0;     // koniec rejestracji nowych uczestników
	}

	// odnawianie siê przedmiotu -> wysy³anie ramki
	if (pMojObiekt->nr_odnowionego_przedm > -1)
	{                             // jeœli min¹³ pewnien okres czasu przedmiot mo¿e zostaæ przywrócony
		Ramka ramka;
		ramka.typ_ramki = ODNOWIENIE_SIE_PRZEDMIOTU; 
		ramka.nr_przedmiotu = pMojObiekt->nr_odnowionego_przedm;
		ramka.iID = pMojObiekt->iID;
		int iRozmiar = multi_send->send_delayed((char*)&ramka,sizeof(Ramka));


		pMojObiekt->nr_odnowionego_przedm = -1;
	}



	// --------------------------------------------------------------------
	// --------------- MIEJSCE NA ALGORYTM STEROWANIA ---------------------
	// (dobór si³y F w granicach (-F_max/2, F_max), k¹ta skrêtu kó³ alfa (-alfa_max, alfa_max) oraz
	// si³y o hamowania ham (0,1) [+ decyzji w zwi¹zku ze wspó³prac¹] w zale¿noœci od sytuacji)
	if (sterowanie_autonomiczne)
	{
		ap->Sterowanie(pMojObiekt, CudzeObiekty, iLiczbaCudzychOb);	
		sprintf(napis2,"F=%f,_ham=%f,_alfa=%f",pMojObiekt->F,pMojObiekt->ham,pMojObiekt->alfa);
	}
	

	// --------------------------------------------------------------------


	// ------------------------------------------------------------------------
	// --------------- MIEJSCE #2 NA NEGOCJACJE HANDLOWE  ---------------------
	// (szczegó³y na stronie w instrukcji do zadania)




	// ------------------------------------------------------------------------
}

// *****************************************************************
// ****    Wszystko co trzeba zrobiæ podczas zamykania aplikacji
// ****    poza grafik¹ 
void ZakonczenieInterakcji()
{
	fprintf(f,"Koniec interakcji\n");
	fclose(f);
}

// Funkcja wysylajaca ramke z przekazem, zwraca zrealizowan¹ wartoœæ przekazu
float WyslaniePrzekazu(int ID_adresata, int typ_przekazu, float wartosc_przekazu)
{
	Ramka ramka;
	ramka.typ_ramki = PRZEKAZ;
	ramka.iID_adresata = ID_adresata;
	ramka.typ_przekazu = typ_przekazu;
	ramka.wartosc_przekazu = wartosc_przekazu;
	ramka.iID = pMojObiekt->iID;

	// tutaj nale¿a³oby uzyskaæ potwierdzenie przekazu zanim sumy zostan¹ odjête
	if (typ_przekazu == GOTOWKA)
	{
		if (pMojObiekt->pieniadze < wartosc_przekazu) 
			ramka.wartosc_przekazu = pMojObiekt->pieniadze;
		pMojObiekt->pieniadze -= ramka.wartosc_przekazu;
		sprintf(napis2,"Przelew_sumy_ %f _na_rzecz_ID_ %d",wartosc_przekazu,ID_adresata);
	}
	else if (typ_przekazu == PALIWO)
	{
		// odszukanie adresata, sprawdzenie czy jest odpowiednio blisko:
		int indeks_adresata = -1;
		for (int i = 0; i < iLiczbaCudzychOb; i++) {
			//if (CudzeObiekty[i]->iID == ID_adresata) {
			if (CudzeObiekty[i]->iID != -1) {	
				sprintf(napis2, "Przelew paliwa to id: %d", CudzeObiekty[i]->iID);
				ramka.iID_adresata = CudzeObiekty[i]->iID;
				indeks_adresata = i; break;
			}
		}

		/* if ((CudzeObiekty[indeks_adresata]->wPol - pMojObiekt->wPol).dlugosc() > CudzeObiekty[indeks_adresata]->dlugosc + pMojObiekt->dlugosc) {
			ramka.wartosc_przekazu = 0;
		} else {
			if (pMojObiekt->ilosc_paliwa < wartosc_przekazu) {
				ramka.wartosc_przekazu = pMojObiekt->ilosc_paliwa;
			}
			pMojObiekt->ilosc_paliwa -= ramka.wartosc_przekazu;
			sprintf(napis2,"Przekazanie_paliwa_w_iloœci_ %f _na_rzecz_ID_ %d",wartosc_przekazu,ID_adresata);
		}*/
	}

	ramka.wartosc_przekazu = wartosc_przekazu;
	if (ramka.wartosc_przekazu > 0)
		int iRozmiar = multi_send->send_delayed((char*)&ramka,sizeof(Ramka));  

	sprintf(napis1, "Przelewam %f paliwa do obiektu o id %d", ramka.wartosc_przekazu, ramka.iID_adresata);
	return ramka.wartosc_przekazu;
}


// ************************************************************************
// ****    Obs³uga klawiszy s³u¿¹cych do sterowania obiektami lub
// ****    widokami 
void KlawiszologiaSterowania(UINT kod_meldunku, WPARAM wParam, LPARAM lParam)
{

	switch (kod_meldunku) 
	{

	case WM_LBUTTONDOWN: //reakcja na lewy przycisk myszki
		{
			int x = LOWORD(lParam);
			int y = HIWORD(lParam);
			if (sterowanie_myszkowe)
				pMojObiekt->F = pMojObiekt->F_max;        // si³a pchaj¹ca do przodu
			else                       // zaznaczanie obiektów
			{
				RECT r;
				//GetWindowRect(okno,&r);
				GetClientRect(okno,&r);
				Wektor3 w = WspolrzedneKursora3D(x,r.bottom - r.top - y);
				//float promien = (w - punkt_klik).dlugosc();
				float odl_min = 1e10;
				long ind_min = -1;
				bool czy_ob_ruch;
				for (long i=0;i<iLiczbaCudzychOb;i++)
				{
					float xx,yy,zz;
					WspolrzedneEkranu(&xx,&yy,&zz, CudzeObiekty[i]->wPol);
					yy = r.bottom - r.top - yy;
					float odl_kw = (xx-x)*(xx-x) + (yy-y)*(yy-y);
					if (odl_min > odl_kw)
					{
						odl_min = odl_kw;
						ind_min = i;
						czy_ob_ruch = 1;
					}
				}

				for (long i=0;i<teren.liczba_przedmiotow;i++)
				{
					float xx,yy,zz;
					WspolrzedneEkranu(&xx,&yy,&zz, teren.p[i].wPol);
					yy = r.bottom - r.top - yy;
					float odl_kw = (xx-x)*(xx-x) + (yy-y)*(yy-y);
					if (odl_min > odl_kw)
					{
						odl_min = odl_kw;
						ind_min = i;
						czy_ob_ruch = 0;
					}
				}
				if (ind_min > -1)
				{
					//fprintf(f,"zaznaczono przedmiot %d pol = (%f, %f, %f)\n",ind_min,teren.p[ind_min].wPol.x,teren.p[ind_min].wPol.y,teren.p[ind_min].wPol.z);
					//teren.p[ind_min].czy_zazn = 1 - teren.p[ind_min].czy_zazn;
					if (czy_ob_ruch)
					{
						CudzeObiekty[ind_min]->czy_zazn = !CudzeObiekty[ind_min]->czy_zazn;
						if (CudzeObiekty[ind_min]->czy_zazn)
							sprintf(napis2, "zaznaczono_ obiekt_ID_%d",CudzeObiekty[ind_min]->iID);
					}
					else
					{
						teren.p[ind_min].czy_zazn = !teren.p[ind_min].czy_zazn;
						if (teren.p[ind_min].czy_zazn)
							sprintf(napis2, "zaznaczono_ przedmiot_%d_wPol_(%f,_%f,_%f)",ind_min,teren.p[ind_min].wPol.x,teren.p[ind_min].wPol.y,teren.p[ind_min].wPol.z);
					}
					//char lan[256];
					//sprintf(lan, "klikniêto w przedmiot %d pol = (%f, %f, %f)\n",ind_min,teren.p[ind_min].wPol.x,teren.p[ind_min].wPol.y,teren.p[ind_min].wPol.z);
					//SetWindowText(okno,lan);
				}
				Wektor3 punkt_klik = WspolrzedneKursora3D(x,r.bottom - r.top - y);

			}
			break;
		}
	case WM_RBUTTONDOWN: //reakcja na prawy przycisk myszki
		{
			int x = LOWORD(lParam);
			int y = HIWORD(lParam);
			if (sterowanie_myszkowe)
				pMojObiekt->F = -pMojObiekt->F_max/2;        // si³a pchaj¹ca do tylu
			break;
		}
	case WM_MBUTTONDOWN: //reakcja na œrodkowy przycisk myszki : uaktywnienie/dezaktywacja sterwania myszkowego
		{
			sterowanie_myszkowe = 1 - sterowanie_myszkowe;
			kursor_x = LOWORD(lParam);
			kursor_y = HIWORD(lParam);
			break;
		}
	case WM_LBUTTONUP: //reakcja na puszczenie lewego przycisku myszki
		{   
			if (sterowanie_myszkowe)
				pMojObiekt->F = 0.0;        // si³a pchaj¹ca do przodu
			break;
		}
	case WM_RBUTTONUP: //reakcja na puszczenie lewy przycisk myszki
		{
			if (sterowanie_myszkowe)
				pMojObiekt->F = 0.0;        // si³a pchaj¹ca do przodu
			break;
		}
	case WM_MOUSEMOVE:
		{
			int x = LOWORD(lParam);
			int y = HIWORD(lParam);
			if (sterowanie_myszkowe)
			{
				float kat_skretu = (float)(kursor_x - x)/20;
				if (kat_skretu > 45) kat_skretu = 45;
				if (kat_skretu < -45) kat_skretu = -45;	
				pMojObiekt->alfa = PI*kat_skretu/180;
			}
			break;
		}
	case WM_MOUSEWHEEL:     // ruch kó³kiem myszy -> przybli¿anie, oddalanie widoku
		{
			int zDelta = GET_WHEEL_DELTA_WPARAM(wParam);  // dodatni do przodu, ujemny do ty³u
			//fprintf(f,"zDelta = %d\n",zDelta);          // zwykle +-120, jak siê bardzo szybko zakrêci to czasmi wyjdzie +-240
			if (zDelta > 0){
				if (oddalenie > 0.5) oddalenie /= 1.2;
				else oddalenie = 0;
			}
			else {
				if (oddalenie > 0) oddalenie *= 1.2;
				else oddalenie = 0.5;
			}


			break;
		}
	case WM_KEYDOWN:
		{

			switch (LOWORD(wParam))
			{
			case VK_SHIFT:
				{
					SHIFTwcisniety = 1; 
					break; 
				}        
			case VK_SPACE:
				{
					pMojObiekt->ham = 1.0;       // stopieñ hamowania (reszta zale¿y od si³y docisku i wsp. tarcia)
					break;                       // 1.0 to maksymalny stopieñ (np. zablokowanie kó³)
				}
			case VK_UP:
				{

					pMojObiekt->F = pMojObiekt->F_max;        // si³a pchaj¹ca do przodu
					break;
				}
			case VK_DOWN:
				{
					pMojObiekt->F = -pMojObiekt->F_max/2;        // sila pchajaca do tylu
					break;
				}
			case VK_LEFT:
				{
					if (SHIFTwcisniety) pMojObiekt->alfa = PI*35/180;
					else pMojObiekt->alfa = PI*15/180;

					break;
				}
			case VK_RIGHT:
				{
					if (SHIFTwcisniety) pMojObiekt->alfa = -PI*35/180;
					else pMojObiekt->alfa = -PI*15/180;
					break;
				}
			case 'W':   // przybli¿enie widoku
				{
					//pol_kamery = pol_kamery - kierunek_kamery*0.3;
					if (oddalenie > 0.5) oddalenie /= 1.2;
					else oddalenie = 0;  
					break; 
				}     
			case 'S':   // oddalenie widoku
				{
					//pol_kamery = pol_kamery + kierunek_kamery*0.3; 
					if (oddalenie > 0) oddalenie *= 1.2;
					else oddalenie = 0.5;   
					break; 
				}    
			case 'Q':   // widok z góry
				{
					pol_kamery = Wektor3(0,100,0);
					kierunek_kamery = Wektor3(0,-1,0.01);
					pion_kamery = Wektor3(0,0,-1);
					break;
				}
			case 'E':   // obrót kamery ku górze (wzglêdem lokalnej osi z)
				{
					kat_kam_z += PI*5/180; 
					break; 
				}    
			case 'D':   // obrót kamery ku do³owi (wzglêdem lokalnej osi z)
				{
					kat_kam_z -= PI*5/180;  
					break;
				}
			case 'A':   // w³¹czanie, wy³¹czanie trybu œledzenia obiektu
				{
					sledzenie = 1 - sledzenie;
					break;
				}
			case 'Z':   // zoom - zmniejszenie k¹ta widzenia
				{
					zoom /= 1.1;
					RECT rc;
					GetClientRect (okno, &rc);
					ZmianaRozmiaruOkna(rc.right - rc.left,rc.bottom-rc.top);
					break;
				}
			case 'X':   // zoom - zwiêkszenie k¹ta widzenia
				{
					zoom *= 1.1;
					RECT rc;
					GetClientRect (okno, &rc);
					ZmianaRozmiaruOkna(rc.right - rc.left,rc.bottom-rc.top);
					break;
				}
			case 'O':   // zanotowanie w pliku po³o¿enia œrodka pojazdu jako miejsca posadzenia drzewa
				{
					//fprintf(f,"p[%d].typ = DRZEWO; p[%d].podtyp = SWIERK; p[%d].wPol = Wektor3(%f,%f,%f); p[%d].do_wziecia = 0; p[%d].wartosc = 8;\n",
					//  nr_miejsca_przedm,nr_miejsca_przedm,nr_miejsca_przedm,pMojObiekt->wPol.x,pMojObiekt->wPol.y,pMojObiekt->wPol.z,
					//  nr_miejsca_przedm,nr_miejsca_przedm);
					fprintf(f,"p[%d].typ = MONETA; p[%d].wPol = Wektor3(%f,%f,%f); p[%d].do_wziecia = 1; p[%d].wartosc = 200;\n",
						nr_miejsca_przedm,nr_miejsca_przedm,pMojObiekt->wPol.x,pMojObiekt->wPol.y,pMojObiekt->wPol.z,
						nr_miejsca_przedm,nr_miejsca_przedm);
					//fprintf(f,"p[%d].typ = BECZKA; p[%d].wPol = Wektor3(%f,%f,%f); p[%d].do_wziecia = 1; p[%d].wartosc = 10;\n",
					//  nr_miejsca_przedm,nr_miejsca_przedm,pMojObiekt->wPol.x,pMojObiekt->wPol.y,pMojObiekt->wPol.z,
					//  nr_miejsca_przedm,nr_miejsca_przedm);
					nr_miejsca_przedm++;
					break;
				}

			case 'F':  // przekazanie 10 kg paliwa pojazdowi, który znajduje siê najbli¿ej
				{
					float min_odleglosc = 1e10;
					int indeks_min = -1;
					for (int i=0;i<iLiczbaCudzychOb;i++)
					{
						if (min_odleglosc > (CudzeObiekty[i]->wPol - pMojObiekt->wPol).dlugosc() )
						{
							min_odleglosc = (CudzeObiekty[i]->wPol - pMojObiekt->wPol).dlugosc();
							indeks_min = i;
						}
					}

					float ilosc_p =  0;
					if (indeks_min > -1)
						ilosc_p = WyslaniePrzekazu(CudzeObiekty[indeks_min]->iID, PALIWO, 10);

					if (ilosc_p == 0) 
						MessageBox(okno,"Paliwa nie da³o siê przekazaæ, bo byæ mo¿e najbli¿szy obiekt ruchomy znajduje siê zbyt daleko.",
						"Nie da³o siê przekazaæ paliwa!",MB_OK);
					break;
				}
			case 'G':  // przekazanie 100 jednostek gotowki pojazdowi, który znajduje siê najbli¿ej
				{
					float min_odleglosc = 1e10;
					int indeks_min = -1;
					for (int i=0;i<iLiczbaCudzychOb;i++)
					{
						if (min_odleglosc > (CudzeObiekty[i]->wPol - pMojObiekt->wPol).dlugosc() )
						{
							min_odleglosc = (CudzeObiekty[i]->wPol - pMojObiekt->wPol).dlugosc();
							indeks_min = i;
						}
					}

					float ilosc_p =  0;
					if (indeks_min > -1)
						ilosc_p = WyslaniePrzekazu(CudzeObiekty[indeks_min]->iID, GOTOWKA, 100);

					if (ilosc_p == 0) 
						MessageBox(okno,"Gotówki nie da³o siê przekazaæ, bo byæ mo¿e najbli¿szy obiekt ruchomy znajduje siê zbyt daleko.",
						"Nie da³o siê przekazaæ gotówki!",MB_OK);
					break;
				}
			case 'Y':
			if (sterowanie_autonomiczne) sterowanie_autonomiczne = false;
		else sterowanie_autonomiczne = true;

				if (sterowanie_autonomiczne)
					sprintf(napis1,"Wlaczenie_autosterowania,_teraz_pojazd_staje_sie_samochodem");
				else
		{
			pMojObiekt->alfa = pMojObiekt->F = pMojObiekt->ham = 0;
					sprintf(napis1,"Wylaczenie_autosterowania");
		}
				break;
			case 'U':
			{
				// przekaz paliwo
				sprintf(napis1, "Przekaz paliwa, posiadam: %d", pMojObiekt->ilosc_paliwa);
				WyslaniePrzekazu(-1, PALIWO, pMojObiekt->ilosc_paliwa / 2.0);
			}
				break;
			case VK_F2:  // sumulacja automatycznego sterowania obiektem w celu jego oceny poza czasem rzeczywistym 
				{
					bool czy_parametry_idealne = true;    // pe³ne umiejêtnoœci, sta³y czas kroku
					Teren t2;
					ObiektRuchomy *Obiekt = new ObiektRuchomy(&t2);
					float krok_czasowy = -1;
					if (czy_parametry_idealne)
					{
						Obiekt->umiejetn_sadzenia = Obiekt->umiejetn_zb_monet = Obiekt->umiejetn_zb_paliwa = 1.0;
						krok_czasowy = krok_czasowy_autopilota;
					}
					else
					{
						Obiekt->umiejetn_sadzenia = pMojObiekt->umiejetn_sadzenia;
						Obiekt->umiejetn_zb_monet = pMojObiekt->umiejetn_zb_monet; 
						Obiekt->umiejetn_zb_paliwa = pMojObiekt->umiejetn_zb_paliwa;
						krok_czasowy = fDt;
					}
					long pieniadze_pocz = Obiekt->pieniadze;

					char lanc[256];
					sprintf(lanc,"Test sterowania autonomicznego dla um.got = %1.1f, um.pal = %1.1f, krok = %f[s], - prosze czekac!",Obiekt->umiejetn_zb_monet,Obiekt->umiejetn_zb_paliwa,krok_czasowy);
					SetWindowText(okno,lanc);
					long t_start = clock();


					ap->TestSterowania(Obiekt, krok_czasowy, czas_testu_autopilota);

					char lan[512],lan1[512];
					sprintf(lan, "Uzyskano %d gotowki w ciagu %f sekund (krok = %f), ilosc paliwa = %f, czy pokazac caly test?", Obiekt->pieniadze - pieniadze_pocz, czas_testu_autopilota, krok_czasowy, Obiekt->ilosc_paliwa);
					sprintf(lan1,"Test autonomicznego sterowania, czas testu = %f s.",(float)(clock()-t_start)/CLOCKS_PER_SEC);
					int wynik = MessageBox(okno,lan,lan1,MB_YESNO);
					if (wynik == 6)
					{
						tryb_pokazu_autopilota = true;	
						biezacy_czas_pokazu_autopilota = 0;
						pMojObiekt->umiejetn_sadzenia = pMojObiekt->umiejetn_zb_monet = pMojObiekt->umiejetn_zb_paliwa = 1.0;
					}
					break;
				}

			} // switch po klawiszach

			break;
		}

	case WM_KEYUP:
		{
			switch (LOWORD(wParam))
			{
			case VK_SHIFT:
				{
					SHIFTwcisniety = 0; 
					break; 
				}        
			case VK_SPACE:
				{
					pMojObiekt->ham = 0.0;
					break;
				}
			case VK_UP:
				{
					pMojObiekt->F = 0.0;

					break;
				}
			case VK_DOWN:
				{
					pMojObiekt->F = 0.0;
					break;
				}
			case VK_LEFT:
				{
					pMojObiekt->alfa = 0;	
					break;
				}
			case VK_RIGHT:
				{
					pMojObiekt->alfa = 0;	
					break;
				}

			}

			break;
		}

	} // switch po komunikatach
}
