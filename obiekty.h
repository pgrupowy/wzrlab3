#include <stdio.h>
#include "kwaternion.h"

#define PI 3.1416
extern class Teren;


struct StanObiektu
{
  int iID;                  // identyfikator obiektu
	Wektor3 wPol;             // polozenie obiektu (�rodka geometrycznego obiektu) 
	kwaternion qOrient;       // orientacja (polozenie katowe)
	Wektor3 wV,wA;            // predkosc, przyspiesznie liniowe
  Wektor3 wV_kat, wA_kat;   // predkosc i przyspeszenie liniowe
	float masa_calk;          // masa ca�kowita (wa�na przy kolizjach)
	long pieniadze;
  float ilosc_paliwa;
	int iID_wlasc;
	int czy_autonom;
};



class ObiektRuchomy
{
public:
	double a, b;
	int iID;                  // identyfikator obiektu
	int closestObjectIndex = -1;
	int agentRole; // 0 - fuel, 1 - money
	// parametry stanu:
	Wektor3 wPol;             // polozenie obiektu
	kwaternion qOrient;       // orientacja (polozenie katowe)
	Wektor3 wV,wA;            // predkosc, przyspiesznie liniowe
	Wektor3 wV_kat, wA_kat;   // predkosc i przyspeszenie liniowe
	long pieniadze;
	float ilosc_paliwa;
  
  // parametry akcji:
  float F;                  // F - si�a pchajaca do przodu (do ty�u ujemna)
  float ham;                // stopie� hamowania Fh_max = tarcie*Fy*ham
	float alfa;               // kat skretu kol w radianach (w lewo - dodatni)
 


  // decyzje zwi�zane z trwa�� wsp�prac� polegaj�c� na dzieleniu si�
  // paliwem i got�wk� p� na p�:
  bool wyslanie_zaproszenia_do_dzielenia;
  bool zgoda_na_dzielenie;
  bool koniec_dzielenia;
  int ID_koalicjanta;       // identyfikator obiektu, z kt�rym chcemy nawi�za� wsp�prac� (-1 z nikim)

  // decyzje zwi�zane z kupnem 10 kg paliwa:
  long cena_od_kupca_paliwa;     // cena zaproponowana przez chc�cego kupi� paliwo
  bool chec_sprzedazy_paliwa;    // ch�� sprzeda�y paliwa po zaproponowanej cenie 
  int ID_kupca_paliwa;           // identyfikator kupca paliwa, gdy podejmiemy decyzj� o sprzeda�y (-1 brak takiej decyzji)
  int ID_przedawcy_paliwa;       // identyfikator sprzedawcy paliwa, gdy podejmiemy decyzj� o jego kupnie

  // decyzje zwi�zane z podniesieniem ci�kiej monety:
  int nr_przedm_do_podniesienia;        // numer przedmiotu do podniesienia
  bool czy_przelew_polowy_wartosci;     // decyzja o przelewie w zwi�zku z pomoc�


	// parametry sta�e:
	float umiejetn_sadzenia,    // umiej�tno�� sadzenia drzew (1-pe�na, 0- brak)
        umiejetn_zb_paliwa,   // umiej�tno�� zbierania paliwa
        umiejetn_zb_monet; 

  bool czy_zazn;            // czy obiekt zaznaczony  

  // parametry fizyczne pojazdu:
	float F_max;
  float alfa_max;
	float Fy;                 // si�a nacisku na podstaw� pojazdu - gdy obiekt styka si� z pod�o�em (od niej zale�y si�a hamowania)
	float masa_wlasna;		  // masa w�asna obiektu (bez paliwa)	
	float masa_calk;          // masa ca�kowita (wa�na przy kolizjach)
	float dlugosc,szerokosc,wysokosc; // rozmiary w kierunku lokalnych osi x,y,z
	float promien;            // promien minimalnej sfery, w ktora wpisany jest obiekt
	float przeswit;           // wysoko�� na kt�rej znajduje si� podstawa obiektu
	float dl_przod;           // odleg�o�� od przedniej osi do przedniego zderzaka 
	float dl_tyl;             // odleg�o�� od tylniej osi do tylniego zderzaka 

  // inne: 
	int iID_kolid;            // identyfikator pojazdu z kt�rym nast�pi�a kolizja (-1  brak kolizji)
	Wektor3 wdV_kolid;        // poprawka pr�dko�ci pojazdu koliduj�cego
	int iID_wlasc; 
	bool czy_autonom;         // czy obiekt jest autonomiczny
	long nr_wzietego_przedm;   // numer wzietego przedmiotu
	float wartosc_wzieta;   // wartosc wzietego przedmiotu
	long nr_odnowionego_przedm; 

	
	float czas_symulacji;     // czas sumaryczny symulacji obiektu   
	Teren *teren;             // wska�nik do terenu, do kt�rego przypisany jest obiekt

public:
	ObiektRuchomy(Teren *t);          // konstruktor
	~ObiektRuchomy();
	void ZmienStan(StanObiektu stan);          // zmiana stanu obiektu
	StanObiektu Stan();        // metoda zwracajaca stan obiektu
  void Symulacja(float dt,ObiektRuchomy *CudzeObiekty[],long iLiczbaCudzychOb);  // symulacja ruchu obiektu w oparciu o biezacy stan, przylozone sily
	                           // oraz czas dzialania sil. Efektem symulacji jest nowy stan obiektu 
	void Rysuj();			   // odrysowanie obiektu					
};

enum typ {MONETA, BECZKA, DRZEWO, BUDYNEK};

enum podtyp {TOPOLA, SWIERK, BAOBAB, FANTAZJA};

struct Przedmiot
{
	Wektor3 wPol;             // polozenie obiektu
	kwaternion qOrient;       // orientacja (polozenie katowe)

	int typ;
	int podtyp;
	long wartosc;             // w zal. od typu nomina� monety /ilosc paliwa, itd.
	float srednica;           // np. grubosc pnia u podstawy, srednica monety
	
	bool do_wziecia;          // czy przedmiot mozna wziac
  bool czy_ja_wzialem;      // czy przedmiot wziety przeze mnie
  bool czy_odnawialny;      // czy mo�e si� odnawia� w tym samym miejscu po pewnym czasie
  long czas_wziecia;        // czas wzi�cia (potrzebny do przywr�cenia)

  bool czy_zazn;            // czy przedmiot jest zaznaczony przez uzytkownika
};

class Teren
{
public:
    float **mapa;          // wysoko�ci naro�nik�w oraz �rodk�w p�l
    float ***d;            // warto�ci wyrazu wolnego z r�wnania p�aszczyzny dla ka�dego tr�jk�ta
    Wektor3 ***Norm;       // normalne do p�aszczyzn tr�jk�t�w
    float rozmiar_pola;    // dlugosc boku kwadratowego pola na mapie
    long lwierszy,lkolumn; // liczba wierszy i kolumn mapy (kwadrat�w na wysoko�� i szeroko��)     
    
    long liczba_przedmiotow;      // liczba przedmiot�w na planszy
    long liczba_przedmiotow_max;  // rozmiar tablicy przedmiot�w

	float czas_odnowy_przedm;     // czas w [s] po kt�rym nast�puje odnowienie si� przedmiotu 

    Przedmiot *p;          // tablica przedmiotow roznego typu
    
    Teren();    
    ~Teren();   
    float Wysokosc(float x,float z);      // okre�lanie wysoko�ci dla punktu o wsp. (x,z) 
    void Rysuj(int szczegolowosc);	      // odrysowywanie terenu (szczeg. pe�na gdy == 1, zminejszona gdy == 0) 
    void PoczatekGrafiki();               // tworzenie listy wy�wietlania
};