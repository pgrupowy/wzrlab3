/*********************************************************************
Symulacja obiekt�w fizycznych ruchomych np. samochody, statki, roboty, itd. 
+ obs�uga obiekt�w statycznych np. teren.
**********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include "obiekty.h"
#include "grafika.h"
#include <cmath>        // std::abs



//#include "wektor.h"
extern FILE *f;
extern char napis1[300];
extern HWND okno;
//extern Teren teren;
//extern int iLiczbaCudzychOb;
//extern ObiektRuchomy *CudzeObiekty[1000]; 
double getDistance(ObiektRuchomy *player, Przedmiot p) {
	Wektor3 diff = p.wPol - player->wPol;
	return sqrt(diff.x*diff.x + diff.z*diff.z);
}

ObiektRuchomy::ObiektRuchomy(Teren *t)             // konstruktor                   
{
  teren = t;

  //iID = (unsigned int)(clock() % 1000);  // identyfikator obiektu
  iID = (unsigned int)(rand() % 1000);  // identyfikator obiektu
  fprintf(f,"Nowy obiekt: iID = %d\n",iID);
  iID_wlasc = iID;           // identyfikator w�a�ciciela obiektu
  czy_autonom = 0;

  pieniadze = 1000;    // np. dolar�w
  ilosc_paliwa = 10.0;   // np. kilogram�w paliwa 

  czas_symulacji = 0;    // symulowany czas rzeczywisty od pocz�tku istnienia obiektu 

  F = alfa  = 0;	      // si�y dzia�aj�ce na obiekt 
  ham = 0;			      // stopie� hamowania

  F_max = 6000;
  alfa_max = PI*45.0/180;
  masa_wlasna = 800.0;     // masa w�asna obiektu [kg] (bez paliwa)
  masa_calk = masa_wlasna + ilosc_paliwa;  // masa ca�kowita
  Fy = masa_wlasna*9.81;    // si�a nacisku na podstaw� obiektu (na ko�a pojazdu)
  dlugosc = 6.0;
  szerokosc = 2.7;
  wysokosc = 1.0;
  przeswit = 0.0;           // wysoko�� na kt�rej znajduje si� podstawa obiektu
  dl_przod = 1.0;           // odleg�o�� od przedniej osi do przedniego zderzaka 
  dl_tyl = 0.2;             // odleg�o�� od tylniej osi do tylniego zderzaka

  iID_kolid = -1;           // na razie brak kolizji 

  //wPol.y = przeswit+wysokosc/2 + 10;
  wPol = Wektor3(220,przeswit+wysokosc/2 + 10,-50);
  promien = sqrt(dlugosc*dlugosc + szerokosc*szerokosc + wysokosc*wysokosc)/2/1.15;
  //wV_kat = Wektor3(0,1,0)*40;  // pocz�tkowa pr�dko�� k�towa (w celach testowych)

  //moment_wziecia = 0;            // czas ostatniego wziecia przedmiotu
  //czas_oczekiwania = 1000000000; // 
  //nr_przedmiotu = -1;

  nr_wzietego_przedm = -1;
  wartosc_wzieta = 0;
  nr_odnowionego_przedm = -1;

  // obr�t obiektu o k�t 30 stopni wzgl�dem osi y:
  kwaternion qObr = AsixToQuat(Wektor3(0,1,0),-40*PI/180.0);
  qOrient = qObr*qOrient;

  // losowanie umiej�tno�ci tak by nie by�o bardzo s�abych i bardzo silnych:
  umiejetn_sadzenia = 0.2 + (float)(rand()%5)/5;
  umiejetn_zb_paliwa =   0.2 + (float)(rand()%5)/5;
  umiejetn_zb_monet =   0.2 + (float)(rand()%5)/5;
  //float suma_um = umiejetn_sadzenia + umiejetn_zb_paliwa + umiejetn_zb_monet;
  //float suma_um_los = 0.7 + 0.8*(float)rand()/RAND_MAX;    // losuje umiejetno�� sumaryczn�
  //umiejetn_sadzenia *= suma_um_los/suma_um;
  //umiejetn_zb_paliwa *= suma_um_los/suma_um;
  //umiejetn_zb_monet *= suma_um_los/suma_um;

  czy_zazn = false;
}

ObiektRuchomy::~ObiektRuchomy()           // destruktor
{}

void ObiektRuchomy::ZmienStan(StanObiektu stan)  // przepisanie podanego stanu 
{                                                // w przypadku obiekt�w, kt�re nie s� symulowane
  this->iID = stan.iID;                        

  this->wPol = stan.wPol;
  this->qOrient = stan.qOrient;
  this->wV = stan.wV;
  this->wA = stan.wA;
  this->wV_kat = stan.wV_kat;
  this->wA_kat = stan.wA_kat;

  this->masa_calk = stan.masa_calk;
  this->ilosc_paliwa = stan.ilosc_paliwa;
  this->pieniadze = stan.pieniadze;
  this->iID_wlasc = stan.iID_wlasc;
  this->czy_autonom = stan.czy_autonom;
}

StanObiektu ObiektRuchomy::Stan()                // metoda zwracaj�ca stan obiektu ��cznie z iID
{
  StanObiektu stan;

  stan.iID = iID;
  stan.qOrient = qOrient;
  stan.wA = wA;
  stan.wA_kat = wA_kat;
  stan.wPol = wPol;
  stan.wV = wV;
  stan.wV_kat = wV_kat;

  stan.masa_calk = masa_calk;
  stan.ilosc_paliwa = ilosc_paliwa;
  stan.pieniadze = pieniadze;
  stan.iID_wlasc = iID_wlasc;
  stan.czy_autonom = czy_autonom;
  return stan;
}

float getAngle(Wektor3 *v1, Wektor3 *v2) {
	double numerator = v1->x*v2->x + v1->z * v2->z;
	double denumerator = sqrt(pow(v1->x, 2.0) + pow(v1->z, 2.0))*sqrt(pow(v2->x, 2.0) + pow(v2->z, 2.0));
	return acos(numerator / denumerator);
}

void ObiektRuchomy::Symulacja(float dt,ObiektRuchomy *CudzeObiekty[],long iLiczbaCudzychOb)          // obliczenie nowego stanu na podstawie dotychczasowego,
{                                                // dzia�aj�cych si� i czasu, jaki up�yn�� od ostatniej symulacji

  if (dt == 0) return;

  czas_symulacji += dt;          // sumaryczny czas wszystkich symulacji obiektu od jego powstania

  float tarcie = 0.7;            // wsp�czynnik tarcia obiektu o pod�o�e 
  float tarcie_obr = tarcie;     // tarcie obrotowe (w szczeg�lnych przypadkach mo�e by� inne ni� liniowe)
  float tarcie_toczne = 0.30;    // wsp�czynnik tarcia tocznego
  float sprezystosc = 0.2;       // wsp�czynnik spr�ysto�ci (0-brak spr�ysto�ci, 1-doskona�a spr�ysto��) 
  float g = 9.81;                // przyspieszenie grawitacyjne
  float m = masa_wlasna + ilosc_paliwa;   // masa calkowita


  // obracam uk�ad wsp�rz�dnych lokalnych wed�ug kwaterniona orientacji:
  Wektor3 w_przod = qOrient.obroc_wektor(Wektor3(1,0,0)); // na razie o� obiektu pokrywa si� z osi� x globalnego uk�adu wsp�rz�dnych (lokalna o� x)
  Wektor3 w_gora = qOrient.obroc_wektor(Wektor3(0,1,0));  // wektor skierowany pionowo w g�r� od podstawy obiektu (lokalna o� y)
  Wektor3 w_prawo = qOrient.obroc_wektor(Wektor3(0,0,1)); // wektor skierowany w prawo (lokalna o� z)

  //fprintf(f,"w_przod = (%f, %f, %f)\n",w_przod.x,w_przod.y,w_przod.z);
  //fprintf(f,"w_gora = (%f, %f, %f)\n",w_gora.x,w_gora.y,w_gora.z);
  //fprintf(f,"w_prawo = (%f, %f, %f)\n",w_prawo.x,w_prawo.y,w_prawo.z);

  //fprintf(f,"|w_przod|=%f,|w_gora|=%f,|w_prawo|=%f\n",w_przod.dlugosc(),w_gora.dlugosc(),w_prawo.dlugosc()  );
  //fprintf(f,"ilo skalar = %f,%f,%f\n",w_przod^w_prawo,w_przod^w_gora,w_gora^w_prawo  );
  //fprintf(f,"w_przod = (%f, %f, %f) w_gora = (%f, %f, %f) w_prawo = (%f, %f, %f)\n",
  //           w_przod.x,w_przod.y,w_przod.z,w_gora.x,w_gora.y,w_gora.z,w_prawo.x,w_prawo.y,w_prawo.z);


  // rzutujemy wV na sk�adow� w kierunku przodu i pozosta�e 2 sk�adowe
  // sk�adowa w bok jest zmniejszana przez si�� tarcia, sk�adowa do przodu
  // przez si�� tarcia tocznego
  Wektor3 wV_wprzod = w_przod*(wV^w_przod),
    wV_wprawo = w_prawo*(wV^w_prawo),
    wV_wgore = w_gora*(wV^w_gora); 

  // dodatkowa normalizacja likwidujaca blad numeryczny:
  if (wV.dlugosc() > 0)
  {
    float blad_dlugosci = (wV_wprzod + wV_wprawo + wV_wgore).dlugosc() / wV.dlugosc();
    wV_wprzod = wV_wprzod / blad_dlugosci;
    wV_wprawo = wV_wprawo / blad_dlugosci;
    wV_wgore = wV_wgore / blad_dlugosci;
  }

  // rzutujemy pr�dko�� k�tow� wV_kat na sk�adow� w kierunku przodu i pozosta�e 2 sk�adowe
  Wektor3 wV_kat_wprzod = w_przod*(wV_kat^w_przod),
    wV_kat_wprawo = w_prawo*(wV_kat^w_prawo),
    wV_kat_wgore = w_gora*(wV_kat^w_gora);         


  // ograniczenia 
  if (F > F_max) F = F_max;
  if (F < -F_max/2) F = -F_max/2;
  if (alfa > alfa_max) alfa = alfa_max ;
  if (alfa < -alfa_max) alfa = -alfa_max ;

  // obliczam promien skr�tu pojazdu na podstawie k�ta skr�tu k�, a nast�pnie na podstawie promienia skr�tu
  // obliczam pr�dko�� k�tow� (UPROSZCZENIE! pomijam przyspieszenie k�towe oraz w�a�ciw� trajektori� ruchu)
  if (Fy > 0)
  {
    float V_kat_skret = 0;
    if (alfa != 0)
    {   
      float Rs = sqrt(dlugosc*dlugosc/4 + (fabs(dlugosc/tan(alfa)) + szerokosc/2)*(fabs(dlugosc/tan(alfa)) + szerokosc/2));
      V_kat_skret = wV_wprzod.dlugosc()*(1.0/Rs);
    }	
    Wektor3 wV_kat_skret = w_gora*V_kat_skret*(alfa > 0 ? 1 : -1);
    Wektor3 wV_kat_wgore2 = wV_kat_wgore + wV_kat_skret;
    if (wV_kat_wgore2.dlugosc() <= wV_kat_wgore.dlugosc()) // skr�t przeciwdzia�a obrotowi
    {
      if (wV_kat_wgore2.dlugosc() > V_kat_skret)
        wV_kat_wgore = wV_kat_wgore2; 
      else  
        wV_kat_wgore = wV_kat_skret;
    }    
    else    
    {
      if (wV_kat_wgore.dlugosc() < V_kat_skret)
        wV_kat_wgore = wV_kat_skret;

    }

    // tarcie zmniejsza pr�dko�� obrotow� (UPROSZCZENIE! zamiast masy winienem wykorzysta� moment bezw�adno�ci)     
    float V_kat_tarcie = Fy*tarcie_obr*dt/m/1.0;      // zmiana pr. k�towej spowodowana tarciem
    float V_kat_wgore = wV_kat_wgore.dlugosc() - V_kat_tarcie;
    if (V_kat_wgore < V_kat_skret) V_kat_wgore = V_kat_skret;        // tarcie nie mo�e spowodowa� zmiany zwrotu wektora pr. k�towej
    wV_kat_wgore = wV_kat_wgore.znorm()*V_kat_wgore;                     
  }    


  Fy = m*g*w_gora.y;                      // si�a docisku do pod�o�a 
  if (Fy < 0 ) Fy = 0;
  // ... trzeba j� jeszcze uzale�ni� od tego, czy obiekt styka si� z pod�o�em!
  float Fh = Fy*tarcie*ham;                  // si�a hamowania (UP: bez uwzgl�dnienia po�lizgu)

  float V_wprzod = wV_wprzod.dlugosc();// - dt*Fh/m - dt*tarcie_toczne*Fy/m;
  if (V_wprzod < 0) V_wprzod = 0;

  float V_wprawo = wV_wprawo.dlugosc();// - dt*tarcie*Fy/m;
  if (V_wprawo < 0) V_wprawo = 0;


  // wjazd lub zjazd: 
  //wPol.y = teren.Wysokosc(wPol.x,wPol.z);   // najprostsze rozwi�zanie - obiekt zmienia wysoko�� bez zmiany orientacji

  // 1. gdy wjazd na wkl�s�o��: wyznaczam wysoko�ci terenu pod naro�nikami obiektu (ko�ami), 
  // sprawdzam kt�ra tr�jka
  // naro�nik�w odpowiada najni�ej po�o�onemu �rodkowi ci�ko�ci, gdy przylega do terenu
  // wyznaczam pr�dko�� podbicia (wznoszenia �rodka pojazdu spowodowanego wkl�s�o�ci�) 
  // oraz pr�dko�� k�tow�
  // 2. gdy wjazd na wypuk�o�� to si�a ci�ko�ci wywo�uje obr�t przy du�ej pr�dko�ci liniowej

  // punkty zaczepienia k� (na wysoko�ci pod�ogi pojazdu):
  Wektor3 P = wPol + w_przod*(dlugosc/2-dl_przod) - w_prawo*szerokosc/2 - w_gora*wysokosc/2,
    Q = wPol + w_przod*(dlugosc/2-dl_przod) + w_prawo*szerokosc/2 - w_gora*wysokosc/2,
    R = wPol + w_przod*(-dlugosc/2+dl_tyl) - w_prawo*szerokosc/2 - w_gora*wysokosc/2,
    S = wPol + w_przod*(-dlugosc/2+dl_tyl) + w_prawo*szerokosc/2 - w_gora*wysokosc/2;

  // pionowe rzuty punkt�w zacz. k� pojazdu na powierzchni� terenu:  
  Wektor3 Pt = P, Qt = Q, Rt = R, St = S;
  Pt.y = teren->Wysokosc(P.x,P.z); Qt.y = teren->Wysokosc(Q.x,Q.z);  
  Rt.y = teren->Wysokosc(R.x,R.z); St.y = teren->Wysokosc(S.x,S.z);   
  Wektor3 normPQR = normalna(Pt,Rt,Qt), normPRS = normalna(Pt,Rt,St), normPQS = normalna(Pt,St,Qt),
    normQRS = normalna(Qt,Rt,St);   // normalne do p�aszczyzn wyznaczonych przez tr�jk�ty

  //fprintf(f,"P.y = %f, Pt.y = %f, Q.y = %f, Qt.y = %f, R.y = %f, Rt.y = %f, S.y = %f, St.y = %f\n",
  //  P.y, Pt.y, Q.y, Qt.y, R.y,Rt.y, S.y, St.y);

  float sryPQR = ((Qt^normPQR) - normPQR.x*wPol.x - normPQR.z*wPol.z)/normPQR.y, // wys. �rodka pojazdu
    sryPRS = ((Pt^normPRS) - normPRS.x*wPol.x - normPRS.z*wPol.z)/normPRS.y, // po najechaniu na skarp� 
    sryPQS = ((Pt^normPQS) - normPQS.x*wPol.x - normPQS.z*wPol.z)/normPQS.y, // dla 4 tr�jek k�
    sryQRS = ((Qt^normQRS) - normQRS.x*wPol.x - normQRS.z*wPol.z)/normQRS.y;
  float sry = sryPQR; Wektor3 norm = normPQR;
  if (sry > sryPRS) {sry = sryPRS; norm = normPRS;}  
  if (sry > sryPQS) {sry = sryPQS; norm = normPQS;}
  if (sry > sryQRS) {sry = sryQRS; norm = normQRS;}  // wyb�r tr�jk�ta o �rodku najni�ej po�o�onym    



  Wektor3 wV_kat_wpoziomie = Wektor3(0,0,0);
  // jesli kt�re� z k� jest poni�ej powierzchni terenu
  if ((P.y <= Pt.y + wysokosc/2+przeswit)||(Q.y <= Qt.y  + wysokosc/2+przeswit)||
    (R.y <= Rt.y  + wysokosc/2+przeswit)||(S.y <= St.y  + wysokosc/2+przeswit))
  {   
    // obliczam powsta�� pr�dko�� k�tow� w lokalnym uk�adzie wsp�rz�dnych:      
    Wektor3 wobrot = -norm.znorm()*w_gora*0.6;  
    wV_kat_wpoziomie = wobrot/dt; 
  }    

  Wektor3 wAg = Wektor3(0,-1,0)*g;    // przyspieszenie grawitacyjne

  // jesli wiecej niz 2 kola sa na ziemi, to przyspieszenie grawitacyjne jest rownowazone przez opor gruntu:
  if ((P.y <= Pt.y + wysokosc/2+przeswit)+(Q.y <= Qt.y  + wysokosc/2+przeswit)+
    (R.y <= Rt.y  + wysokosc/2+przeswit)+(S.y <= St.y  + wysokosc/2+przeswit) > 2)
  {	
    wAg = wAg + 
      w_gora*(w_gora^wAg)*-1; //przyspieszenie wynikaj�ce z si�y oporu gruntu
  }
  else   // w przeciwnym wypadku brak sily docisku 
    Fy = 0;



  // sk�adam z powrotem wektor pr�dko�ci k�towej: 
  //wV_kat = wV_kat_wgore + wV_kat_wprawo + wV_kat_wprzod;  
  wV_kat = wV_kat_wgore + wV_kat_wpoziomie;


  float h = sry+wysokosc/2+przeswit - wPol.y;  // r�nica wysoko�ci jak� trzeba pokona�  
  float V_podbicia = 0;
  if ((h > 0)&&(wV.y <= 0.01))
    V_podbicia = 0.5*sqrt(2*g*h);  // pr�dko�� spowodowana podbiciem pojazdu przy wje�d�aniu na skarp� 
  if (h > 0) wPol.y = sry+wysokosc/2+przeswit;  

  // lub  w przypadku zag��bienia si� 
  //fprintf(f,"sry = %f, wPol.y = %f, dt = %f\n",sry,wPol.y,dt);  
  //fprintf(f,"normPQR.y = %f, normPRS.y = %f, normPQS.y = %f, normQRS.y = %f\n",normPQR.y,normPRS.y,normPQS.y,normQRS.y); 

  Wektor3 dwPol = wV*dt;//wA*dt*dt/2; // czynnik bardzo ma�y - im wi�ksza cz�stotliwo�� symulacji, tym mniejsze znaczenie 
  wPol = wPol + dwPol;  

  // korekta po�o�enia w przypadku terenu cyklicznego:
  if (wPol.x < -teren->rozmiar_pola*teren->lkolumn/2) wPol.x += teren->rozmiar_pola*teren->lkolumn;
  else if (wPol.x > teren->rozmiar_pola*(teren->lkolumn-teren->lkolumn/2)) wPol.x -= teren->rozmiar_pola*teren->lkolumn;
  if (wPol.z < -teren->rozmiar_pola*teren->lwierszy/2) wPol.z += teren->rozmiar_pola*teren->lwierszy;
  else if (wPol.z > teren->rozmiar_pola*(teren->lwierszy-teren->lwierszy/2)) wPol.z -= teren->rozmiar_pola*teren->lwierszy;

  // Sprawdzenie czy obiekt mo�e si� przemie�ci� w zadane miejsce: Je�li nie, to 
  // przemieszczam obiekt do miejsca zetkni�cia, wyznaczam nowe wektory pr�dko�ci
  // i pr�dko�ci k�towej, a nast�pne obliczam nowe po�o�enie na podstawie nowych
  // pr�dko�ci i pozosta�ego czasu. Wszystko powtarzam w p�tli (pojazd znowu mo�e 
  // wjecha� na przeszkod�). Problem z zaokr�glonymi przeszkodami - konieczne 
  // wyznaczenie minimalnego kroku.


  Wektor3 wV_pop = wV;  

  // sk�adam pr�dko�ci w r�nych kierunkach oraz efekt przyspieszenia w jeden wektor:    (problem z przyspieszeniem od si�y tarcia -> to przyspieszenie 
  //      mo�e dzia�a� kr�cej ni� dt -> trzeba to jako� uwzgl�dni�, inaczej poazd b�dzie w�ykowa�)
  wV = wV_wprzod.znorm()*V_wprzod + wV_wprawo.znorm()*V_wprawo + wV_wgore + 
    Wektor3(0,1,0)*V_podbicia + wA*dt;
  // usuwam te sk�adowe wektora pr�dko�ci w kt�rych kierunku jazda nie jest mo�liwa z powodu
  // przesk�d:
  // np. je�li pojazd styka si� 3 ko�ami z nawierzchni� lub dwoma ko�ami i �rodkiem ci�ko�ci to
  // nie mo�e mie� pr�dko�ci w d� pod�ogi
  if ((P.y <= Pt.y  + wysokosc/2+przeswit)||(Q.y <= Qt.y  + wysokosc/2+przeswit)||  
    (R.y <= Rt.y  + wysokosc/2+przeswit)||(S.y <= St.y  + wysokosc/2+przeswit))    // je�li pojazd styka si� co najm. jednym ko�em
  {
    Wektor3 dwV = wV_wgore + w_gora*(wA^w_gora)*dt;
    if ((w_gora.znorm() - dwV.znorm()).dlugosc() > 1 )  // je�li wektor skierowany w d� pod�ogi
      wV = wV - dwV;
  }

  /*fprintf(f," |wV_wprzod| %f -> %f, |wV_wprawo| %f -> %f, |wV_wgore| %f -> %f |wV| %f -> %f\n",
  wV_wprzod.dlugosc(), (wV_wprzod.znorm()*V_wprzod).dlugosc(), 
  wV_wprawo.dlugosc(), (wV_wprawo.znorm()*V_wprawo).dlugosc(),
  wV_wgore.dlugosc(), (wV_wgore.znorm()*wV_wgore.dlugosc()).dlugosc(),
  wV_pop.dlugosc(), wV.dlugosc()); */

  // sk�adam przyspieszenia liniowe od si� nap�dzaj�cych i od si� oporu: 
  wA = (w_przod*F)/m*(Fy>0)*(ilosc_paliwa>0)  // od si� nap�dzaj�cych
    - wV_wprzod.znorm()*(Fh/m + tarcie_toczne*Fy/m)*(V_wprzod>0.01) // od hamowania i tarcia tocznego (w kierunku ruchu)
    - wV_wprawo.znorm()*tarcie*Fy/m*(V_wprawo>0.01)    // od tarcia w kierunku prost. do kier. ruchu
    + wAg;           // od grawitacji


  // utrata paliwa:
  ilosc_paliwa -= (fabs(F))*(Fy>0)*dt/20000;      
  if (ilosc_paliwa < 0 ) ilosc_paliwa = 0;     
  masa_calk = masa_wlasna + ilosc_paliwa;


  // obliczenie nowej orientacji:
  Wektor3 w_obrot = wV_kat*dt;// + wA_kat*dt*dt/2;    
  kwaternion q_obrot = AsixToQuat(w_obrot.znorm(),w_obrot.dlugosc());
  //fprintf(f,"w_obrot = (x=%f, y=%f, z=%f) \n",w_obrot.x, w_obrot.y, w_obrot.z );
  //fprintf(f,"q_obrot = (w=%f, x=%f, y=%f, z=%f) \n",q_obrot.w, q_obrot.x, q_obrot.y, q_obrot.z );
  qOrient = q_obrot*qOrient; 	
  //fprintf(f,"Pol = (%f, %f, %f) V = (%f, %f, %f) A = (%f, %f, %f) V_kat = (%f, %f, %f) ID = %d\n",
  //  wPol.x,wPol.y,wPol.z,wV.x,wV.y,wV.z,wA.x,wA.y,wA.z,wV_kat.x,wV_kat.y,wV_kat.z,iID);

  // wykrywanie kolizji z drzewami:
  for (long i=0;i<teren->liczba_przedmiotow;i++)    
    if (teren->p[i].typ == DRZEWO)
    {
      // bardzo duze uproszczenie -> traktuje pojazd jako kul�
      Wektor3 wPolDrz = teren->p[i].wPol; 
      wPolDrz.y = (wPolDrz.y + teren->p[i].wartosc > wPol.y ? wPol.y : wPolDrz.y + teren->p[i].wartosc);                        
      if  ((wPolDrz - wPol).dlugosc() < promien + teren->p[i].srednica/2)  // jesli kolizja
      {             
        // od wektora predkosci odejmujemy jego rzut na kierunek od punktu styku do osi drzewa:
        // jesli pojazd juz wjechal w drzewo, to nieco zwiekszamy poprawke     
        // punkt styku znajdujemy laczac krawedz pojazdu z osia drzewa odcinkiem
        // do obu prostopadlym      
        Wektor3 dP = (wPolDrz - wPol).znorm();   // wektor, w ktorego kierunku ruch jest niemozliwy
        float k = wV^dP;
        if (k > 0)     // jesli jest skladowa predkosci w strone drzewa
        {
          wV = wV - dP*k*(1 + sprezystosc);  // odjecie skladowej + odbicie sprezyste 
          //wV_kat = 
        }  
      }                              
    }	

    // kolizje z innymi obiektami
    if (iID_kolid == iID) // kto� o numerze iID_kolid wykry� kolizj� z naszym pojazdem i poinformowa� nas o tym

    {
      //fprintf(f,"ktos wykryl kolizje - modyf. predkosci\n",iID_kolid);
      wV = wV + wdV_kolid;   // modyfikuje pr�dko�� o wektor obliczony od drugiego (�yczliwego) uczestnika
      iID_kolid = -1;

    }
    else      
    {
      for (long i=0;i<iLiczbaCudzychOb;i++)
      {
        ObiektRuchomy *inny = CudzeObiekty[i];      

        if ((wPol - inny->wPol).dlugosc() < 2*promien)  // je�li kolizja (zak�adam, �e drugi obiekt ma taki sam promie�
        {
          // zderzenie takie jak w symulacji kul 
          Wektor3 norm_pl_st = (wPol - inny->wPol).znorm();    // normalna do p�aszczyzny stycznej - kierunek odbicia
          float m1 = masa_calk, m2 = inny->masa_calk;          // masy obiekt�w
          float W1 = wV^norm_pl_st, W2 = inny->wV^norm_pl_st;  // wartosci pr�dko�ci
          if (W2>W1)      // je�li obiekty si� przybli�aj�
          {

            float Wns = (m1*W1 + m2*W2)/(m1+m2);        // pr�dko�� po zderzeniu ca�kowicie niespr�ystym
            float W1s = ((m1-m2)*W1 + 2*m2*W2)/(m1+m2), // pr�dko�� po zderzeniu ca�kowicie spr�ystym
              W2s = ((m2-m1)*W2 + 2*m1*W1)/(m1+m2);
            float W1sp = Wns +(W1s-Wns)*sprezystosc;    // pr�dko�� po zderzeniu spr�ysto-plastycznym
            float W2sp = Wns +(W2s-Wns)*sprezystosc;

            wV = wV + norm_pl_st*(W1sp-W1);    // poprawka pr�dko�ci (zak�adam, �e inny w przypadku drugiego obiektu zrobi to jego w�asny symulator) 
            iID_kolid = inny->iID;
            wdV_kolid = norm_pl_st*(W2sp-W2);
            //fprintf(f,"wykryto i zreal. kolizje z %d W1=%f,W2=%f,W1s=%f,W2s=%f,m1=%f,m2=%f\n",iID_kolid,W1,W2,W1s,W2s,m1,m2); 
          }
          //if (fabs(W2 - W1)*dt < (wPol - inny->wPol).dlugosc() < 2*promien) wV = wV + norm_pl_st*(W1sp-W1)*2;
        }
      }
    } // do else

    // sprawdzam, czy nie najecha�em na monet� lub beczk� z paliwem. 
    // zak�adam, �e w jednym cylku symulacji mog� wzi�� maksymalnie jeden przedmiot
    for (long i=0;i<teren->liczba_przedmiotow;i++)
    {
      if ((teren->p[i].do_wziecia == 1)&&
        ((teren->p[i].wPol - wPol + Wektor3(0,wPol.y - teren->p[i].wPol.y,0)).dlugosc() < promien))
      {
        float odl_nasza = (teren->p[i].wPol - wPol + Wektor3(0,wPol.y - teren->p[i].wPol.y,0)).dlugosc();

        long wartosc = teren->p[i].wartosc;
        wartosc_wzieta = -1;

        if (teren->p[i].typ == MONETA)
        {
          bool mozna_wziac = false;
          // przy du�ej warto�ci nie mog� samodzielnie podnie�� pieni��ka bez bratniej pomocy innego pojazdu
          // odleg�o�� bratniego pojazdu od pieni�dza nie mo�e by� mniejsza od naszej odleg�o�ci, gdy� wtedy
          // to ten inny pojazd zgarnie monet�:
          if (wartosc >= 2000)
          {
            bool bratnia_pomoc = false;    
            int kto_pomogl = -1;
            for (long k=0;k<iLiczbaCudzychOb;k++)
            {
              float odl_bratnia = (CudzeObiekty[k]->wPol - teren->p[i].wPol).dlugosc();
              if ((odl_bratnia < CudzeObiekty[k]->promien*2)&&(odl_nasza < odl_bratnia))
              {
                bratnia_pomoc = true;
                kto_pomogl = CudzeObiekty[k]->iID;
              }
            }

            if (!bratnia_pomoc)
            {
              sprintf(napis1,"nie_mozna_wziac_tak_ciezkiego_pieniazka,_chyba_ze_ktos_podjedzie_i_pomoze.");
              mozna_wziac = false;
            }
            else
            {
              sprintf(napis1,"pojazd_o_ID_%d_pomogl_wziac_monete_o_wartosci_%d",kto_pomogl,wartosc);
              mozna_wziac = true;
            }
          }
          else
            mozna_wziac = true;

          if (mozna_wziac)
          {
            wartosc_wzieta = (float)wartosc*umiejetn_zb_monet;   
            pieniadze += (long)wartosc_wzieta;  
          }

          //sprintf(napis2,"Wziecie_gotowki_o_wartosci_ %d",wartosc);
        } // je�li to moneta
		else if (teren->p[i].typ == BECZKA)
        {
          wartosc_wzieta = (float)wartosc*umiejetn_zb_paliwa; 
          ilosc_paliwa += wartosc_wzieta; 
          //sprintf(napis2,"Wziecie_paliwa_w_ilosci_ %d",wartosc);
        }

        if (wartosc_wzieta > 0)
        {
          teren->p[i].do_wziecia = 0;
          teren->p[i].czy_ja_wzialem = 1;
          teren->p[i].czas_wziecia = czas_symulacji;

          // zapis informacji, by przekaza� j� innym aplikacjom:
          nr_wzietego_przedm = i;
        }
      }
      else if ((teren->p[i].do_wziecia == 0)&&(teren->p[i].czy_ja_wzialem)&&(teren->p[i].czy_odnawialny)&&
        (czas_symulacji - teren->p[i].czas_wziecia >= teren->czas_odnowy_przedm))
      {                              // je�li min�� pewnien okres czasu przedmiot mo�e zosta� przywr�cony
        teren->p[i].do_wziecia = 1;
		teren->p[i].czy_ja_wzialem = 0;
        nr_odnowionego_przedm = i;
      }
    }
	//this->ilosc_paliwa = 10.2;
	float speed = sqrt(pow(wV.x, 2.0) + pow(wV.y, 2.0) + pow(wV.z, 2.0));
	//sprintf(napis1, "Current speed: %f Umiejetnosc zbierania: %.3f", speed, umiejetn_zb_monet);
}


void ObiektRuchomy::Rysuj()
{
  glPushMatrix();

  glTranslatef(wPol.x,wPol.y+przeswit,wPol.z);

  kwaternion k = qOrient.AsixAngle();
  //fprintf(f,"kwaternion = [%f, %f, %f], w = %f\n",qOrient.x,qOrient.y,qOrient.z,qOrient.w);
  //fprintf(f,"os obrotu = [%f, %f, %f], kat = %f\n",k.x,k.y,k.z,k.w*180.0/PI);

  glRotatef(k.w*180.0/PI,k.x,k.y,k.z);
  glPushMatrix();
  glTranslatef(-dlugosc/2,-wysokosc/2,-szerokosc/2);

  glScalef(dlugosc,wysokosc,szerokosc);
  glCallList(Cube);
  glPopMatrix();
  if (this->czy_zazn)
  {
    float w = 1.1;
    glTranslatef(-dlugosc/2*w,-wysokosc/2*w,-szerokosc/2*w);
    glScalef(dlugosc*w,wysokosc*w,szerokosc*w);
    GLfloat Surface[] = { 2.0f, 0.0f, 0.0f, 1.0f};
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, Surface);
    glCallList(Cube_skel);
  }

  GLfloat Surface[] = { 2.0f, 2.0f, 1.0f, 1.0f};
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, Surface);
  glRasterPos2f(0.30,1.20);
  glPrint("%d",iID ); 
  glPopMatrix();
}



//**********************
//   Obiekty nieruchome
//**********************
Teren::Teren()
{

  rozmiar_pola = 30;         // d�ugo�� boku kwadratu w [m]           

  float rozmiar_pola_pop = 35;  // poprzedni, oryginalny rozmiar pola [m]
  float t[][44] = { {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}, // ostatni element nieu�ywany
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0, -1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 20, 20,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0, -1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 80,170,170,200,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  2,  5,  5,  2,  0,  0,  0,  0,  0,  0,  0,120,220,250,200,200,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  3,  6,  9, 12, 12,  4,  0,  0,  0,  0,  0, 40,130,200,250,200,150,  0,  0,  0,  0,  0,  0,  0,100,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  2,  2,  0,  0, 14,  9,  4,  0,  0,  0, 20, 40,120,200,220,150,150,  0,  0,  0,  0, 50, 50,300,300,300,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  1,  1,  0,  1,  1,  1,  2,  2,  0,  0,  8,  4,  0,  0,  0,  0, 20, 40, 90,120,170,  0,  0,  0,  0,  0,  0, 60,300,350,330,300,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0, -1,  2,  0, -3,  0,  2,  1,  2,  0,  0,  0,  0,  0,  0, 10, 20, 30, 40, 50,100,140,  0,  0,  0,  0,  0,  0, 50,300,300,300,150, 50,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0, -1,  2,  0,  0,  0,  0, -1, -1,  2,  0,  0,  0,  0,  0,  0,  0, 10, 10, 40, 70,100,110,  0,  0,  0,  0,  0,  0, 50, 40,300,200, 50, 50,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  1,  0,  0, -1,  0,  0,  0,  2,  0,  0,  0,  0,  0,  0,  0,  0,  0, 10, 10, 10, 90, 90,  0,  0,  0,  0,  0,  0,  0,100, 40,100, 50, 50,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  1,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,-10,-10, 10, 10,  0,  0,  0,  0,  0,  0,  0,100,100,  0,100, 70, 40,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  1,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,-10, 40, 40,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 70, 70, 30,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 70, 70,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 70, 20, 20,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 70, 70,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 20,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0,  0, -2, -2, -2, -2,  0,  0,  0,  0,  0,  0,  0, -1, -1,  0,  0, 70,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0,  0, -6, -5, -5, -3, -3,  0,  0,  0,  0,  0,  0, -2, -2, -1,  0, 70, 70,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0, -7, -6, -3, -3, -5, -4,  0,  0,  0,  0,  0, -1, -3, -3, -2,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0, -8, -8,  0,  0,  0, -4, -2,  0,  0,  0,  0,  0, -2, -3, -3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0, -8,  0,  0,  0,  0, -2,  0,  0,  0,  0,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},  
  {0,  0,  0,  0,-40,-40,-40,-10,-40,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,-40,-40,-40,-40,-40,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,-40,-40,-40,-40,-40,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 10,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,-40,-40,-40,-40,-40,  0,  0, 10,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 10, 10,  0,  0,  0,  0,  0,  0},        
  {0,  0,  0,  0,-40,-40,-40,-40,-40,  0,  0,  8, 10,-10,  0,  0,  0,  0,  7,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 10, 10,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,-40,-40,-40,-40,  0,  8, 10,-20,-10,  0,  0,  0,  7,  7,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 10, 10,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,-40,-40,-40,-40,  0,  8, 16, 10,-10,  0,  0,  0,  0,  7,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 10,  0,  0, 10, 10, 10,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  2, 10, 10, 10, 10,  0,  0,  0,  0,  0,  0,  0,-20,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 10, 10,  0, 10, 10, 10,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  2, 10, 10, 10,  3,  0,  0,  0,  0,  0,  0,-20,-20,  0,  0,  0,  0,  0,  0,  0,  0,  0, 10, 10, 10, 20, 20, 10,  5,  5,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  2, 10, 10,  5,  0,  0,  0,  0,  0,-40,-40,-40,-20,-30,-30,  0,  0,  0,  0,  0,  0,  0,  0, 10, 20, 20, 20, 10,  5,  5,  0,  0,  0,  0},
  {0,  0,  0,  0,  0, -3,  0,  0,-10,-10,  0,  2, 10,  5,  0,  0,  1,  0,  0,-40,-40,-40,-40,-30,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 20, 20, 20, 10,  5,  5,  0,  0,  0,  0},
  {0,  0,  0,  0,  0, -3,  0,-13,-10, -6,  0,  0,  5,  0,  0,  1,  3,  0,  0,-40,-40,-40,-40,-30,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 20, 20, 20, 10,  5,  5,  5,  0,  0,  0},
  {0,  0,  0,  0,  0,  0, -3,  0,-18,-16,  0,  0,  0,  0,  0,  0,  2,  3,  5,-40,-40,-40,-40,-30,-20,-20,  0,  0,  0,  0,  0,  0,  0,  0, 20, 20, 20, 10,  5,  5,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0, -3,  0,  0,  0,  0,  0,  0,  0,  0,  1,  2,  4,  0,-40,-40,-40,-40,-30,-20,-20,-20,  0,  0,  0,  0,  3,  5, 10, 20, 20, 20, 10,  5,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0, -3,  0,  0,  0,  0,  0,  0,  0,  0,  2,  3,  5,-40,-40,-40,-40,-30,-20,-20,-20,  0,  0,  0,  0,  3,  5, 10, 20, 20, 20, 10,  5,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0, -3,  0,  0,  0,  0,  0,  0,  0,  1,  2,  0,  0,  0,-40,-40,-30,-30,-30,-20,-20,  0,  0,  0,  0,  3,  5, 10, 20, 20, 20, 10,  5,  0, 20, 20,  0,  0},
  {0,  0,  0,  0,  0,  0,  0, -3, -3,  0,  0,  0,  0,  0,  0,  0,  4,  4,  0,  0,  0,-20,-20,-10,-10,-10,-10,  0,  0,  0,  0,  0,  3,  0, 20, 20, 20, 10,  0,  0, 20, 20,  0,  0},
  {0,  0,  3,  0,  0,  0, -3, -5, -3,  0,  0,  0,  0,  0,  0,  2,  4,  2,  0,  0,  0,  0,-20,-10, -5, -5, -5,  0,  0,  0,  0,  0,  0,  0, 20, 20, 20, 10,  0,  0, 20, 20,  0,  0},
  {0,  0,  3,  0,  0,  0, -3, -5, -3,  0,  0,  0,  0,  0,  0,  0,  4,  4,  0,  0,  0,  0,  0,  0, -5, -5, -5,  0,  0,  0,  0,  0,  0,  0, 20, 20, 30, 30, 30, 20, 20, 20, 20,  0},
  {0,  0,  3,  0,  0, -3, -3, -3,  0,  0,  0,  0,  0,  0,  0,  0,  2,  0,  0,  0,  0,  0,  0,  0, -5, -5, -5,  0,  0,  0,  0,  0,  0, 20, 20, 40, 40, 40, 40, 40, 40, 40, 40,  0},
  {0,  0,  0,  0,  0, -3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -5, -5, -5,  0,  0,  0,  0,  0,  0, 20, 30, 40, 40, 60, 60, 60, 60, 40, 40,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 20, 20, 30, 40, 40, 60, 60, 60, 60, 40, 40,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0, 20, 20, 30, 40, 40, 60, 60, 60, 60, 40, 40,  0},
  {0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  0,-50,  0,  0,  0,  0,  1, -1,  0,  0,  0,  3,  3,  0,  0,  0,  0,  0,  0,  0,  0,  0, 30, 20, 30, 40, 60, 60, 60, 60, 60, 40, 40,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,-20,-50,  0,  0,  0,  0,  1, -1,  0,  0,  1,  5,  8,  0,  0,  0,  0,  0,  0,  0,  0, 30, 30, 40, 60, 60, 60, 60, 60, 60, 60, 40,  0},
  {0,  0, 10,  0,  0,  0,  2,  2,  2,  1,-20,-20,-30,  0,  0,  0,  0,  1, -1,  0,  0,  2,  5,  9,  0,  0,  0,  0,  0,  0,  0, 20, 30, 40, 60, 60,100,100,100, 60, 60, 60, 40,  0},
  {0,  0, 10,  0,  0,  0,  0,  0,  0,  0,  0,-20,-10,  0,  0,  0,  0,  0,  1, -1,  0,  0,  2,  5,  7,  0,  0,  0,  0,  0,  0, 20, 30, 40, 60,100,100,100,100,100, 60, 60, 40,  0},
  {0,  0, 10,  0,  0,  4,  4,  2,  3,  2,  1, -5,  0,  0,  0,  0,  0,  0,  1, -1,  0,  0,  2,  4,  0,  0,  0,  0,  0,  0,  0, 20, 30, 40, 60,100,100,100,120,100,100, 60, 40,  0},
  {0,  0, 10,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 20, 30, 40, 60,100,100, 80, 80,100,100, 60, 40,  0},
  {0,  0, 10,  0,  0,  4,  3,  2,  2,  2,  2,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 30, 40, 60,100,100,100, 80,100,100, 60, 40,  0},
  {0,  0, 10,  0,  0,  0,  0,0.5,  1,  1,  0,  0,  0,  0,  0,  0,-30,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 40, 60, 60,100,100,100,100, 60, 60, 40,  0},
  {0,  0, 10,  0,  0,  4,  4,  2,  3,  1,  1,  1,  0,  0,  0,-30,-30,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 20, 60, 60,100,100, 60, 60, 60, 40,  0},          
  {0,  0, 10,  0,  0,  0,  0,  0,  0,  1,  1,  0,  0,  0,  0,-30,-30,-30,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 20, 60, 60, 60, 60, 60, 60, 60, 40,  0},
  {0,  0, 10,  0,  0,  5,  4,  2,  2,  1,  1,  1,  0,  0,  0,-30,-30,-25,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 20, 20, 20, 40, 20, 40, 40, 40, 40,  0},  
  {0,  0, 10,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,-25,-22,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 20, 20, 40, 20, 20, 40, 40,  0,  0},
  {0,  0, 10,  0,  0,-20,60,-20,  0,  0,  0,  0,  0,  0,  0, 10,  0,-22,-20,  0,  0, -3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 20, 20, 20,  0,  0,  0,  0},
  {0,  0, 10,  0,  0,  0, 70,60,-20,  0,  0,  0,  0,  0,  0, 10, 10,  0,-19,-18,  0, -6, -3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 20, 20, 20,  0,  0,  0,  0},
  {0,  0, 10,  0,  0,  0, 65,50,  0,  0,  0,  0,  0,  0,  5, 10,  0,  0,-16,-13, -8, -6,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 20, 20,  0,  0,  0,  0,  0},
  {0,  0, 10,  0,  0,  0,  0,-20,  0,  0,  0,  0,  0,  0,  2,  5,  0,  0,  0,-13,-10, -8,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 20,  0,  0,  0,  0,  0,  0},
  {0,  0, 10,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  2,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0, 10,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0, 10,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0, 10,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,-10,-20,-60,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,-10,-20,-30,-60,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,-10,-40,-90,-60,-60,  0,  0,  0,  0,  0,  0,  0, 10, 10, 10, 10, 10,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,-20,-40,-90,-90,-60,  0,  0,  0,  0,  0,  0, 10, 10, 10, 10, 10, 10,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,-10,-40,-90,-90,-90,-60,  0,  0,  0, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,-10,-50,-90,-90,-90,-60,  0,  0,  0, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,  0,  0,  0,  0,  0,  0,  0,  0, 40, 40,-20,-10,  0,  0,  0,  0,  0},
  {0,-10,-50,-70,-90,-60,-40,  0,  0,  0, 10, 10, 10, 20, 20, 20, 20, 30, 30, 40, 40, 50, 50, 70, 70, 10, 10,  0,  0,  0,  0,  0,  0,  0,  0,  0, 40,-20,-10,  0,  0,  0,  0,  0},
  {0,-10,-20,-40,-40,-40,-40,  0,  0,  0, 10, 10, 10, 20, 20, 20, 20, 30, 30, 40, 40, 50, 50, 70, 70, 10, 10,  0,  0,  0,  0,  0,  0,  0,  0,  0, 40, 40,  0,  0,  0,  0,  0,  0},
  {0,-10,-20,-20,-30,-20,-20,-10,  0,  0, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 40,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,-10,-20,-20,-10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,-10,-10,-10,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
  {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}};

  lkolumn = 43;         // o 1 mniej, gdy� kolumna jest r�wnowa�na ci�gowi kwadrat�w 
  lwierszy = sizeof(t)/sizeof(float)/(lkolumn+1)/2-1;                 
  mapa = new float*[lwierszy*2+1];
  for (long i=0;i<lwierszy*2+1;i++) {
    mapa[i] = new float[lkolumn+1];
    for (long j=0;j<lkolumn+1;j++) mapa[i][j] = t[i][j];
  }  	

  for (long i=0;i<lwierszy*2+1;i++)
    for (long j=0;j<lkolumn+1;j++)
      mapa[i][j] /= 3;

  d = new float**[lwierszy];
  for (long i=0;i<lwierszy;i++) {
    d[i] = new float*[lkolumn];
    for (long j=0;j<lkolumn;j++) d[i][j] = new float[4];
  }    
  Norm = new Wektor3**[lwierszy];
  for (long i=0;i<lwierszy;i++) {
    Norm[i] = new Wektor3*[lkolumn];
    for (long j=0;j<lkolumn;j++) Norm[i][j] = new Wektor3[4];
  }    

  fprintf(f,"mapa terenu: lwierszy = %d, lkolumn = %d rozmiar_mapy = %d\n",lwierszy,lkolumn,sizeof(t));

  liczba_przedmiotow_max = 1000;
  p = new Przedmiot[liczba_przedmiotow_max];
  p[0].typ = MONETA; p[0].wPol = Wektor3(10,0,10); p[0].do_wziecia = 1; p[0].wartosc = 100;
  p[1].typ = MONETA; p[1].wPol = Wektor3(10,0,-10); p[1].do_wziecia = 1; p[1].wartosc = 100;
  p[2].typ = MONETA; p[2].wPol = Wektor3(-10,0,-20); p[2].do_wziecia = 1; p[2].wartosc = 100;
  p[3].typ = MONETA; p[3].wPol = Wektor3(60,0,-60); p[3].do_wziecia = 1; p[3].wartosc = 500;
  p[4].typ = MONETA; p[4].wPol = Wektor3(-40,0,-20); p[4].do_wziecia = 1; p[4].wartosc = 200;
  p[5].typ = MONETA; p[5].wPol = Wektor3(40,0,50); p[5].do_wziecia = 1; p[5].wartosc = 200;
  p[6].typ = DRZEWO; p[6].podtyp = TOPOLA; p[6].wPol = Wektor3(-40,0,50); p[6].do_wziecia = 0; p[6].wartosc = 10;
  p[7].typ = DRZEWO; p[7].podtyp = TOPOLA; p[7].wPol = Wektor3(40,0,55); p[7].do_wziecia = 0; p[7].wartosc = 14;
  p[8].typ = DRZEWO; p[8].podtyp = TOPOLA; p[8].wPol = Wektor3(45,0,55); p[8].do_wziecia = 0; p[8].wartosc = 10;
  p[9].typ = DRZEWO; p[9].podtyp = TOPOLA; p[9].wPol = Wektor3(45,0,37); p[9].do_wziecia = 0; p[9].wartosc = 8;
  p[10].typ = DRZEWO; p[10].podtyp = SWIERK; p[10].wPol = Wektor3(45,0,5); p[10].do_wziecia = 0; p[10].wartosc = 7;
  p[11].typ = DRZEWO; p[11].podtyp = SWIERK; p[11].wPol = Wektor3(38,0,7); p[11].do_wziecia = 0; p[11].wartosc = 8;
  p[12].typ = DRZEWO; p[12].podtyp = SWIERK; p[12].wPol = Wektor3(8,0,57); p[12].do_wziecia = 0; p[12].wartosc = 8;
  p[13].typ = DRZEWO; p[13].podtyp = SWIERK; p[13].wPol = Wektor3(-2,0,60); p[13].do_wziecia = 0; p[13].wartosc = 8;
  p[14].typ = DRZEWO; p[14].podtyp = SWIERK; p[14].wPol = Wektor3(-8,0,52); p[14].do_wziecia = 0; p[14].wartosc = 12;
  p[15].typ = DRZEWO; p[15].podtyp = TOPOLA; p[15].wPol = Wektor3(107,0,-65); p[15].do_wziecia = 0; p[15].wartosc = 18;
  p[16].typ = DRZEWO; p[16].podtyp = TOPOLA; p[16].wPol = Wektor3(110,0,-75); p[16].do_wziecia = 0; p[16].wartosc = 17;
  p[17].typ = DRZEWO; p[17].podtyp = TOPOLA; p[17].wPol = Wektor3(110,0,-85); p[17].do_wziecia = 0; p[17].wartosc = 19;
  p[19].typ = MONETA; p[19].wPol = Wektor3(80,0,-97); p[19].do_wziecia = 1; p[19].wartosc = 1000;
  p[20].typ = MONETA; p[20].wPol = Wektor3(-30,0,-78); p[20].do_wziecia = 1; p[20].wartosc = 200;
  p[21].typ = MONETA; p[21].wPol = Wektor3(40,0,-88); p[21].do_wziecia = 1; p[21].wartosc = 100;
  p[22].typ = MONETA; p[22].wPol = Wektor3(-70,0,-88); p[22].do_wziecia = 1; p[22].wartosc = 100;
  p[23].typ = MONETA; p[23].wPol = Wektor3(-70,0,-68); p[23].do_wziecia = 1; p[23].wartosc = 100;
  p[24].typ = DRZEWO; p[24].podtyp = SWIERK; p[24].wPol = Wektor3(-30,0,-35); p[24].do_wziecia = 0; p[24].wartosc = 8;
  p[25].typ = DRZEWO; p[25].podtyp = SWIERK; p[25].wPol = Wektor3(-25,0,-25); p[25].do_wziecia = 0; p[25].wartosc = 12;
  p[26].typ = MONETA; p[26].wPol = Wektor3(-37,0,-34); p[26].do_wziecia = 1; p[26].wartosc = 100;
  p[27].typ = DRZEWO; p[27].podtyp = TOPOLA; p[27].wPol = Wektor3(140,0,120); p[27].do_wziecia = 0; p[27].wartosc = 22;
  p[28].typ = DRZEWO; p[28].podtyp = TOPOLA; p[28].wPol = Wektor3(113,0,-97); p[28].do_wziecia = 0; p[28].wartosc = 19;
  p[29].typ = BECZKA; p[29].wPol = Wektor3(-90,0,-88); p[29].do_wziecia = 1; p[29].wartosc = 10;
  p[30].typ = BECZKA; p[30].wPol = Wektor3(-100,0,-68); p[30].do_wziecia = 1; p[30].wartosc = 10;
  p[31].typ = BECZKA; p[31].wPol = Wektor3(120,0,-65); p[31].do_wziecia = 1; p[31].wartosc = 10;
  p[32].typ = BECZKA; p[32].wPol = Wektor3(130,0,-99); p[32].do_wziecia = 1; p[32].wartosc = 50;
  p[33].typ = BECZKA; p[33].wPol = Wektor3(150,0,-78); p[33].do_wziecia = 1; p[33].wartosc = 20;
  p[34].typ = BECZKA; p[34].wPol = Wektor3(-110,0,-130); p[34].do_wziecia = 1; p[34].wartosc = 10;
  p[35].typ = BECZKA; p[35].wPol = Wektor3(123,0,-35); p[35].do_wziecia = 1; p[35].wartosc = 10;
  p[36].typ = BECZKA; p[36].wPol = Wektor3(137,0,-110); p[36].do_wziecia = 1; p[36].wartosc = 40;
  p[37].typ = BECZKA; p[37].wPol = Wektor3(140,0,-118); p[37].do_wziecia = 1; p[37].wartosc = 30;
  p[38].typ = BECZKA; p[38].wPol = Wektor3(12,0,-165); p[38].do_wziecia = 1; p[38].wartosc = 10;
  p[39].typ = BECZKA; p[39].wPol = Wektor3(17,0,-110); p[39].do_wziecia = 1; p[39].wartosc = 20;
  p[40].typ = BECZKA; p[40].wPol = Wektor3(14,0,-118); p[40].do_wziecia = 1; p[40].wartosc = 10;
  p[41].typ = MONETA; p[41].wPol = Wektor3(145,0,-12); p[41].do_wziecia = 1; p[41].wartosc = 1000;
  p[42].typ = MONETA; p[42].wPol = Wektor3(147,0,-200); p[42].do_wziecia = 1; p[42].wartosc = 200;
  p[43].typ = MONETA; p[43].wPol = Wektor3(140,0,123); p[43].do_wziecia = 1; p[43].wartosc = 200;
  p[44].typ = MONETA; p[44].wPol = Wektor3(137,0,134); p[44].do_wziecia = 1; p[44].wartosc = 200;
  p[45].typ = DRZEWO; p[45].podtyp = TOPOLA; p[45].wPol = Wektor3(178,0,-194); p[45].do_wziecia = 0; p[45].wartosc = 22;
  p[46].typ = DRZEWO; p[46].podtyp = TOPOLA; p[46].wPol = Wektor3(180,0,-188); p[46].do_wziecia = 0; p[46].wartosc = 19;
  p[47].typ = BECZKA; p[47].wPol = Wektor3(-190,0,158); p[47].do_wziecia = 1; p[47].wartosc = 20;
  p[48].typ = BECZKA; p[48].wPol = Wektor3(-176,0,165); p[48].do_wziecia = 1; p[48].wartosc = 20;
  p[49].typ = BECZKA; p[49].wPol = Wektor3(-140,0,-68); p[49].do_wziecia = 1; p[49].wartosc = 20;
  p[50].typ = BECZKA; p[50].wPol = Wektor3(-136,0,-55); p[50].do_wziecia = 1; p[50].wartosc = 20;
  p[51].typ = MONETA; p[51].wPol = Wektor3(31,0,204); p[51].do_wziecia = 1; p[51].wartosc = 100;
  p[52].typ = MONETA; p[52].wPol = Wektor3(31,0,255); p[52].do_wziecia = 1; p[52].wartosc = 50;
  p[53].typ = MONETA; p[53].wPol = Wektor3(-71,0,40); p[53].do_wziecia = 1; p[53].wartosc = 100;
  p[54].typ = MONETA; p[54].wPol = Wektor3(31,0,75); p[54].do_wziecia = 1; p[54].wartosc = 100;
  p[55].typ = DRZEWO; p[55].podtyp = TOPOLA; p[55].wPol = Wektor3(200,0,-194); p[55].do_wziecia = 0; p[55].wartosc = 20;
  p[56].typ = DRZEWO; p[56].podtyp = TOPOLA; p[56].wPol = Wektor3(320,0,74); p[56].do_wziecia = 0; p[56].wartosc = 20;
  p[57].typ = DRZEWO; p[57].podtyp = TOPOLA; p[57].wPol = Wektor3(320,0,74); p[57].do_wziecia = 0; p[57].wartosc = 20;
  p[58].typ = DRZEWO; p[58].podtyp = TOPOLA; p[58].wPol = Wektor3(328,0,74); p[58].do_wziecia = 0; p[58].wartosc = 24;
  p[59].typ = DRZEWO; p[59].podtyp = SWIERK; p[59].wPol = Wektor3(340,0,64); p[59].do_wziecia = 0; p[59].wartosc = 18;
  p[60].typ = DRZEWO; p[60].podtyp = TOPOLA; p[60].wPol = Wektor3(320,0,84); p[60].do_wziecia = 0; p[60].wartosc = 29;
  p[61].typ = MONETA; p[61].wPol = Wektor3(300,0,94); p[61].do_wziecia = 1; p[61].wartosc = 200;
  p[62].typ = MONETA; p[62].wPol = Wektor3(290,0,107); p[62].do_wziecia = 1; p[62].wartosc = 200;
  p[63].typ = DRZEWO; p[63].podtyp = SWIERK; p[63].wPol = Wektor3(310,0,110); p[63].do_wziecia = 0; p[63].wartosc = 23;
  p[64].typ = DRZEWO; p[64].podtyp = SWIERK; p[64].wPol = Wektor3(290,0,110); p[64].do_wziecia = 0; p[64].wartosc = 20;
  p[65].typ = DRZEWO; p[65].podtyp = BAOBAB; p[65].wPol = Wektor3(29,0,44); p[65].do_wziecia = 0; p[65].wartosc = 23;
  p[66].typ = DRZEWO; p[66].podtyp = BAOBAB; p[66].wPol = Wektor3(-60, 0, 100); p[66].do_wziecia = 0; p[66].wartosc = 10;
  p[67].typ = DRZEWO; p[67].podtyp = FANTAZJA; p[67].wPol = Wektor3(290,0,44); p[67].do_wziecia = 0; p[67].wartosc = 23;
  p[68].typ = DRZEWO; p[68].podtyp = FANTAZJA; p[68].wPol = Wektor3(-90, 0, 10); p[68].do_wziecia = 0; p[68].wartosc = 10;
  p[69].typ = DRZEWO; p[69].podtyp = TOPOLA; p[69].wPol = Wektor3(632.472717,0.912881,-302.502563); p[69].do_wziecia = 0; p[69].wartosc = 9;
  p[70].typ = DRZEWO; p[70].podtyp = TOPOLA; p[70].wPol = Wektor3(652.267273,0.913161,-312.163757); p[70].do_wziecia = 0; p[70].wartosc = 10;
  p[71].typ = DRZEWO; p[71].podtyp = TOPOLA; p[71].wPol = Wektor3(661.970154,0.913277,-319.453369); p[71].do_wziecia = 0; p[71].wartosc = 15;
  p[72].typ = DRZEWO; p[72].podtyp = TOPOLA; p[72].wPol = Wektor3(669.822388,0.913401,-329.818817); p[72].do_wziecia = 0; p[72].wartosc = 15;
  p[73].typ = DRZEWO; p[73].podtyp = TOPOLA; p[73].wPol = Wektor3(677.585083,0.913581,-347.711243); p[73].do_wziecia = 0; p[73].wartosc = 10;
  p[74].typ = DRZEWO; p[74].podtyp = TOPOLA; p[74].wPol = Wektor3(681.690002,0.913748,-365.708588); p[74].do_wziecia = 0; p[74].wartosc = 10;
  p[75].typ = DRZEWO; p[75].podtyp = TOPOLA; p[75].wPol = Wektor3(679.837463,0.913954,-386.691559); p[75].do_wziecia = 0; p[75].wartosc = 12;
  p[76].typ = DRZEWO; p[76].podtyp = TOPOLA; p[76].wPol = Wektor3(677.326782,0.914083,-398.845245); p[76].do_wziecia = 0; p[76].wartosc = 14;
  p[77].typ = DRZEWO; p[77].podtyp = TOPOLA; p[77].wPol = Wektor3(672.983459,0.914193,-408.143097); p[77].do_wziecia = 0; p[77].wartosc = 10;
  p[78].typ = DRZEWO; p[78].podtyp = TOPOLA; p[78].wPol = Wektor3(660.354431,0.914386,-422.346954); p[78].do_wziecia = 0; p[78].wartosc = 10;
  p[79].typ = DRZEWO; p[79].podtyp = TOPOLA; p[79].wPol = Wektor3(638.273071,0.914658,-435.320465); p[79].do_wziecia = 0; p[79].wartosc = 13;
  p[80].typ = DRZEWO; p[80].podtyp = TOPOLA; p[80].wPol = Wektor3(624.956970,0.914813,-437.299652); p[80].do_wziecia = 0; p[80].wartosc = 10;
  p[81].typ = DRZEWO; p[81].podtyp = TOPOLA; p[81].wPol = Wektor3(610.622009,0.914985,-434.782990); p[81].do_wziecia = 0; p[81].wartosc = 10;
  p[82].typ = DRZEWO; p[82].podtyp = TOPOLA; p[82].wPol = Wektor3(595.776917,0.915195,-425.152985); p[82].do_wziecia = 0; p[82].wartosc = 10;
  p[83].typ = DRZEWO; p[83].podtyp = TOPOLA; p[83].wPol = Wektor3(585.950867,0.915412,-408.537964); p[83].do_wziecia = 0; p[83].wartosc = 8;
  p[84].typ = DRZEWO; p[84].podtyp = TOPOLA; p[84].wPol = Wektor3(583.834900,0.915658,-380.728729); p[84].do_wziecia = 0; p[84].wartosc = 10;
  p[85].typ = DRZEWO; p[85].podtyp = TOPOLA; p[85].wPol = Wektor3(598.869812,0.915953,-343.837219); p[85].do_wziecia = 0; p[85].wartosc = 10;
  p[86].typ = DRZEWO; p[86].podtyp = TOPOLA; p[86].wPol = Wektor3(-624.297791,0.917550,-162.224823); p[86].do_wziecia = 0; p[86].wartosc = 7;
  p[87].typ = DRZEWO; p[87].podtyp = TOPOLA; p[87].wPol = Wektor3(-547.940491,-12.365116,-201.055618); p[87].do_wziecia = 0; p[87].wartosc = 10;
  p[88].typ = DRZEWO; p[88].podtyp = TOPOLA; p[88].wPol = Wektor3(-547.151794,-12.365116,-218.333481); p[88].do_wziecia = 0; p[88].wartosc = 11;
  p[89].typ = DRZEWO; p[89].podtyp = TOPOLA; p[89].wPol = Wektor3(-554.100281,-12.365116,-236.128677); p[89].do_wziecia = 0; p[89].wartosc = 10;
  p[90].typ = MONETA; p[90].wPol = Wektor3(226.183517,0.905881,-131.977966); p[90].do_wziecia = 1; p[90].wartosc = 200;
  p[91].typ = MONETA; p[91].wPol = Wektor3(263.419556,0.905952,-141.098480); p[91].do_wziecia = 1; p[91].wartosc = 2000;
  p[92].typ = MONETA; p[92].wPol = Wektor3(287.104889,0.905990,-130.649017); p[92].do_wziecia = 1; p[92].wartosc = 1000;
  p[93].typ = MONETA; p[93].wPol = Wektor3(335.474335,0.906081,-78.665756); p[93].do_wziecia = 1; p[93].wartosc = 200;
  p[94].typ = MONETA; p[94].wPol = Wektor3(364.682526,1.929753,-46.732376); p[94].do_wziecia = 1; p[94].wartosc = 200;
  p[95].typ = MONETA; p[95].wPol = Wektor3(463.006592,7.967625,3.318682); p[95].do_wziecia = 1; p[95].wartosc = 200;
  p[96].typ = MONETA; p[96].wPol = Wektor3(626.966797,1.285740,16.154631); p[96].do_wziecia = 1; p[96].wartosc = 200;
  p[97].typ = MONETA; p[97].wPol = Wektor3(608.149109,20.925455,76.088387); p[97].do_wziecia = 1; p[97].wartosc = 200;
  p[98].typ = MONETA; p[98].wPol = Wektor3(599.040894,36.439148,181.299286); p[98].do_wziecia = 1; p[98].wartosc = 200;
  p[99].typ = MONETA; p[99].wPol = Wektor3(597.937317,38.559483,186.615601); p[99].do_wziecia = 1; p[99].wartosc = 200;
  p[100].typ = MONETA; p[100].wPol = Wektor3(-570.924561,-12.592191,-228.966690); p[100].do_wziecia = 1; p[100].wartosc = 200;
  p[101].typ = MONETA; p[101].wPol = Wektor3(-553.789063,-12.488989,-229.971313); p[101].do_wziecia = 1; p[101].wartosc = 200;
  p[102].typ = MONETA; p[102].wPol = Wektor3(-541.527283,-12.488867,-222.314514); p[102].do_wziecia = 1; p[102].wartosc = 200;
  p[103].typ = MONETA; p[103].wPol = Wektor3(-533.953857,-12.488703,-212.829834); p[103].do_wziecia = 1; p[103].wartosc = 500;
  p[104].typ = MONETA; p[104].wPol = Wektor3(-373.647888,0.983305,-266.223328); p[104].do_wziecia = 1; p[104].wartosc = 200;
  p[105].typ = MONETA; p[105].wPol = Wektor3(-293.566254,1.095974,-371.887268); p[105].do_wziecia = 1; p[105].wartosc = 200;
  p[106].typ = MONETA; p[106].wPol = Wektor3(39.126923,27.159672,-474.906860); p[106].do_wziecia = 1; p[106].wartosc = 2000;
  p[107].typ = MONETA; p[107].wPol = Wektor3(64.734650,34.732437,-465.925476); p[107].do_wziecia = 1; p[107].wartosc = 2000;
  p[108].typ = DRZEWO; p[108].podtyp = SWIERK; p[108].wPol = Wektor3(63.236191,0.586880,-139.625809); p[108].do_wziecia = 0; p[108].wartosc = 8;
  p[109].typ = DRZEWO; p[109].podtyp = SWIERK; p[109].wPol = Wektor3(66.055931,0.588808,-147.259155); p[109].do_wziecia = 0; p[109].wartosc = 8;
  p[110].typ = DRZEWO; p[110].podtyp = SWIERK; p[110].wPol = Wektor3(70.628593,0.590884,-154.814728); p[110].do_wziecia = 0; p[110].wartosc = 7;
  p[111].typ = DRZEWO; p[111].podtyp = SWIERK; p[111].wPol = Wektor3(74.409485,0.600210,-181.930054); p[111].do_wziecia = 0; p[111].wartosc = 8;
  p[112].typ = DRZEWO; p[112].podtyp = SWIERK; p[112].wPol = Wektor3(56.660011,0.608544,-202.465530); p[112].do_wziecia = 0; p[112].wartosc = 6;
  p[113].typ = DRZEWO; p[113].podtyp = SWIERK; p[113].wPol = Wektor3(44.211227,0.613297,-208.163239); p[113].do_wziecia = 0; p[113].wartosc = 8;
  p[114].typ = DRZEWO; p[114].podtyp = SWIERK; p[114].wPol = Wektor3(28.067364,0.619393,-210.209305); p[114].do_wziecia = 0; p[114].wartosc = 6;
  p[115].typ = DRZEWO; p[115].podtyp = SWIERK; p[115].wPol = Wektor3(14.255757,0.625113,-206.943634); p[115].do_wziecia = 0; p[115].wartosc = 8;
  p[116].typ = DRZEWO; p[116].podtyp = SWIERK; p[116].wPol = Wektor3(-24.169546,0.658252,-215.176407); p[116].do_wziecia = 0; p[116].wartosc = 8;
  p[117].typ = DRZEWO; p[117].podtyp = SWIERK; p[117].wPol = Wektor3(-29.784779,0.663917,-222.947144); p[117].do_wziecia = 0; p[117].wartosc = 5;
  p[118].typ = DRZEWO; p[118].podtyp = SWIERK; p[118].wPol = Wektor3(-33.371540,0.674517,-238.807266); p[118].do_wziecia = 0; p[118].wartosc = 8;
  p[119].typ = DRZEWO; p[119].podtyp = SWIERK; p[119].wPol = Wektor3(-30.641068,0.681490,-249.823990); p[119].do_wziecia = 0; p[119].wartosc = 9;
  p[120].typ = DRZEWO; p[120].podtyp = SWIERK; p[120].wPol = Wektor3(-24.503609,0.688872,-258.525330); p[120].do_wziecia = 0; p[120].wartosc = 8;
  p[121].typ = DRZEWO; p[121].podtyp = SWIERK; p[121].wPol = Wektor3(-20.072397,0.693872,-262.510956); p[121].do_wziecia = 0; p[121].wartosc = 9;
  p[122].typ = DRZEWO; p[122].podtyp = SWIERK; p[122].wPol = Wektor3(-13.873348,0.699406,-266.469513); p[122].do_wziecia = 0; p[122].wartosc = 8;
  p[123].typ = DRZEWO; p[123].podtyp = SWIERK; p[123].wPol = Wektor3(-4.258272,0.705241,-269.988953); p[123].do_wziecia = 0; p[123].wartosc = 10;
  p[124].typ = DRZEWO; p[124].podtyp = SWIERK; p[124].wPol = Wektor3(-1.445199,0.706443,-270.476868); p[124].do_wziecia = 0; p[124].wartosc = 8;
  p[125].typ = DRZEWO; p[125].podtyp = SWIERK; p[125].wPol = Wektor3(9.902552,0.707123,-270.211426); p[125].do_wziecia = 0; p[125].wartosc = 11;
  p[126].typ = DRZEWO; p[126].podtyp = SWIERK; p[126].wPol = Wektor3(22.596554,0.707123,-265.348480); p[126].do_wziecia = 0; p[126].wartosc = 8;
  p[127].typ = DRZEWO; p[127].podtyp = SWIERK; p[127].wPol = Wektor3(50.986397,0.616378,-177.523071); p[127].do_wziecia = 0; p[127].wartosc = 12;
  p[128].typ = DRZEWO; p[128].podtyp = SWIERK; p[128].wPol = Wektor3(41.753319,0.616378,-186.364960); p[128].do_wziecia = 0; p[128].wartosc = 13;
  p[129].typ = DRZEWO; p[129].podtyp = SWIERK; p[129].wPol = Wektor3(36.031311,0.616929,-213.996338); p[129].do_wziecia = 0; p[129].wartosc = 8;
  p[130].typ = DRZEWO; p[130].podtyp = SWIERK; p[130].wPol = Wektor3(35.357979,0.617832,-233.075851); p[130].do_wziecia = 0; p[130].wartosc = 14;
  p[131].typ = DRZEWO; p[131].podtyp = SWIERK; p[131].wPol = Wektor3(23.259836,0.619466,-251.000900); p[131].do_wziecia = 0; p[131].wartosc = 8;
  p[132].typ = DRZEWO; p[132].podtyp = SWIERK; p[132].wPol = Wektor3(-4.325057,0.623508,-252.838715); p[132].do_wziecia = 0; p[132].wartosc = 11;
  p[133].typ = DRZEWO; p[133].podtyp = SWIERK; p[133].wPol = Wektor3(-21.519569,0.628547,-242.970978); p[133].do_wziecia = 0; p[133].wartosc = 8;
  p[134].typ = MONETA; p[134].wPol = Wektor3(24.064184,0.989570,-237.835632); p[134].do_wziecia = 1; p[134].wartosc = 200;
  p[135].typ = MONETA; p[135].wPol = Wektor3(9.181233,0.989570,-237.477585); p[135].do_wziecia = 1; p[135].wartosc = 200;
  p[136].typ = MONETA; p[136].wPol = Wektor3(0.694732,0.989570,-240.591492); p[136].do_wziecia = 1; p[136].wartosc = 200;
  p[137].typ = MONETA; p[137].wPol = Wektor3(-9.977914,0.989570,-247.590973); p[137].do_wziecia = 1; p[137].wartosc = 500;
  p[138].typ = MONETA; p[138].wPol = Wektor3(-17.719433,0.989570,-259.048431); p[138].do_wziecia = 1; p[138].wartosc = 200;
  p[139].typ = MONETA; p[139].wPol = Wektor3(-5.118982,0.989570,-259.506348); p[139].do_wziecia = 1; p[139].wartosc = 200;
  p[140].typ = MONETA; p[140].wPol = Wektor3(3.725217,0.989570,-261.501801); p[140].do_wziecia = 1; p[140].wartosc = 200;
  p[141].typ = MONETA; p[141].wPol = Wektor3(13.561393,0.989570,-260.378967); p[141].do_wziecia = 1; p[141].wartosc = 500;
  p[142].typ = MONETA; p[142].wPol = Wektor3(32.313389,0.989570,-210.849899); p[142].do_wziecia = 1; p[142].wartosc = 200;
  p[143].typ = MONETA; p[143].wPol = Wektor3(37.163082,0.989570,-204.181473); p[143].do_wziecia = 1; p[143].wartosc = 500;
  p[144].typ = MONETA; p[144].wPol = Wektor3(44.427998,0.989570,-197.662552); p[144].do_wziecia = 1; p[144].wartosc = 200;
  p[145].typ = MONETA; p[145].wPol = Wektor3(52.029839,0.989570,-190.263504); p[145].do_wziecia = 1; p[145].wartosc = 100;
  p[146].typ = MONETA; p[146].wPol = Wektor3(57.671402,0.989570,-179.875671); p[146].do_wziecia = 1; p[146].wartosc = 200;
  p[147].typ = MONETA; p[147].wPol = Wektor3(66.966080,0.989570,-152.452698); p[147].do_wziecia = 1; p[147].wartosc = 500;
  p[148].typ = MONETA; p[148].wPol = Wektor3(53.951099,-9.458956,-81.352837); p[148].do_wziecia = 1; p[148].wartosc = 200;
  p[149].typ = MONETA; p[149].wPol = Wektor3(87.066048,-0.568960,46.441082); p[149].do_wziecia = 1; p[149].wartosc = 200;
  p[150].typ = BECZKA; p[150].wPol = Wektor3(454.271973,0.994743,-359.416443); p[150].do_wziecia = 1; p[150].wartosc = 10;
  p[151].typ = BECZKA; p[151].wPol = Wektor3(357.483521,0.994743,-368.162537); p[151].do_wziecia = 1; p[151].wartosc = 10;
  p[152].typ = BECZKA; p[152].wPol = Wektor3(358.063232,0.994743,-381.350708); p[152].do_wziecia = 1; p[152].wartosc = 10;
  p[153].typ = BECZKA; p[153].wPol = Wektor3(351.096191,0.994743,-397.172943); p[153].do_wziecia = 1; p[153].wartosc = 10;
  p[154].typ = BECZKA; p[154].wPol = Wektor3(343.415833,0.994743,-404.362946); p[154].do_wziecia = 1; p[154].wartosc = 10;
  p[155].typ = BECZKA; p[155].wPol = Wektor3(333.758362,0.994743,-409.167542); p[155].do_wziecia = 1; p[155].wartosc = 10;
  p[156].typ = BECZKA; p[156].wPol = Wektor3(324.660889,0.994743,-411.284393); p[156].do_wziecia = 1; p[156].wartosc = 20;
  p[157].typ = BECZKA; p[157].wPol = Wektor3(314.794067,0.994743,-411.157623); p[157].do_wziecia = 1; p[157].wartosc = 10;
  p[158].typ = BECZKA; p[158].wPol = Wektor3(304.039001,0.994743,-407.027435); p[158].do_wziecia = 1; p[158].wartosc = 10;
  p[159].typ = BECZKA; p[159].wPol = Wektor3(397.891418,0.994743,-401.791962); p[159].do_wziecia = 1; p[159].wartosc = 10;
  p[160].typ = BECZKA; p[160].wPol = Wektor3(394.121216,0.994743,-385.307983); p[160].do_wziecia = 1; p[160].wartosc = 20;
  p[161].typ = BECZKA; p[161].wPol = Wektor3(399.643433,0.994743,-370.202087); p[161].do_wziecia = 1; p[161].wartosc = 10;
  p[162].typ = BECZKA; p[162].wPol = Wektor3(309.874878,0.994743,-353.348633); p[162].do_wziecia = 1; p[162].wartosc = 10;
  p[163].typ = BECZKA; p[163].wPol = Wektor3(322.580017,0.994743,-341.732452); p[163].do_wziecia = 1; p[163].wartosc = 10;
  p[164].typ = BECZKA; p[164].wPol = Wektor3(346.656250,0.994743,-337.008698); p[164].do_wziecia = 1; p[164].wartosc = 10;
  p[165].typ = BECZKA; p[165].wPol = Wektor3(370.063446,23.621290,-453.724457); p[165].do_wziecia = 1; p[165].wartosc = 50;
  p[166].typ = BECZKA; p[166].wPol = Wektor3(257.352173,29.021656,-456.398956); p[166].do_wziecia = 1; p[166].wartosc = 50;
  p[167].typ = MONETA; p[167].wPol = Wektor3(52.029839, 0.989570, 140.263504); p[167].do_wziecia = 1; p[167].wartosc = 5000;


  liczba_przedmiotow = 168;

  //Przedmiot _p = {{Wektor3(1,2,3),kwaternion(0,0,0,1),MONETA,100,1}}; 
  for (long i = 0;i<liczba_przedmiotow;i++) 
  {
    
    p[i].wPol.x *= rozmiar_pola/rozmiar_pola_pop;
    p[i].wPol.z *= rozmiar_pola/rozmiar_pola_pop;
	if ((p[i].typ == MONETA) || (p[i].typ == BECZKA))
	{
		p[i].wPol.y = 1.7 + Wysokosc(p[i].wPol.x, p[i].wPol.z);
		p[i].czy_odnawialny = 1;
	}
	else
		p[i].czy_odnawialny = 0;
    p[i].czy_zazn = 0;
	p[i].czy_ja_wzialem = 0;
  }

  czas_odnowy_przedm = 140;   // czas [s] po jakim przedmiot jest odnawiany
}

Teren::~Teren()
{
  for (long i = 0;i< lwierszy*2+1;i++) delete mapa[i];             
  delete mapa;   
  for (long i=0;i<lwierszy;i++)  {
    for (long j=0;j<lkolumn;j++) delete d[i][j];
    delete d[i];
  }
  delete d;  
  for (long i=0;i<lwierszy;i++)  {
    for (long j=0;j<lkolumn;j++) delete Norm[i][j];
    delete Norm[i];
  }
  delete Norm;  

  delete p;         
}

float Teren::Wysokosc(float x,float z)      // okre�lanie wysoko�ci dla punktu o wsp. (x,z) 
{

  float pocz_x = -rozmiar_pola*lkolumn/2,     // wsp�rz�dne lewego g�rnego kra�ca terenu
    pocz_z = -rozmiar_pola*lwierszy/2;        

  long k = (long)((x - pocz_x)/rozmiar_pola), // wyznaczenie wsp�rz�dnych (w,k) kwadratu
    w = (long)((z - pocz_z)/rozmiar_pola);
  if ((k < 0)||(k >= lkolumn)||(w < 0)||(w >= lwierszy)) return 0;  // je�li poza map�

  // wyznaczam punkt B - �rodek kwadratu oraz tr�jk�t, w kt�rym znajduje si� punkt
  // (rysunek w Teren::PoczatekGrafiki())
  Wektor3 B = Wektor3(pocz_x + (k+0.5)*rozmiar_pola, mapa[w*2+1][k], pocz_z + (w+0.5)*rozmiar_pola); 
  enum tr{ABC=0,ADB=1,BDE=2,CBE=3};       // tr�jk�t w kt�rym znajduje si� punkt 
  int trojkat=0; 
  if ((B.x > x)&&(fabs(B.z - z) < fabs(B.x - x))) trojkat = ADB;
  else if ((B.x < x)&&(fabs(B.z - z) < fabs(B.x - x))) trojkat = CBE;
  else if ((B.z > z)&&(fabs(B.z - z) > fabs(B.x - x))) trojkat = ABC;
  else trojkat = BDE;

  // wyznaczam normaln� do p�aszczyzny a nast�pnie wsp�czynnik d z r�wnania p�aszczyzny
  float dd = d[w][k][trojkat];
  Wektor3 N = Norm[w][k][trojkat];
  float y;
  if (N.y > 0) y = (-dd - N.x*x - N.z*z)/N.y;
  else y = 0;

  //fprintf(f,"kwadr = (w=%d,k=%d), trojk = %d,  (x,z)=(%f,%f) y = %f\n",w,k,trojkat,x,z,y);
  //fprintf(f,"kwadr = (w=%d,k=%d), trojk = %d, N=(%f, %f, %f)\n",w,k,trojkat,N.x,N.y,N.z);

  return y;    
}

void Teren::PoczatekGrafiki()
{
  // tworze list� wy�wietlania rysuj�c poszczeg�lne pola mapy za pomoc� tr�jk�t�w 
  // (po 4 tr�jk�ty na ka�de pole):
  enum tr{ABC=0,ADB=1,BDE=2,CBE=3};       
  float pocz_x = -rozmiar_pola*lkolumn/2,     // wsp�rz�dne lewego g�rnego kra�ca terenu
    pocz_z = -rozmiar_pola*lwierszy/2;        
  Wektor3 A,B,C,D,E,N;      
  glNewList(PowierzchniaTerenu,GL_COMPILE);
  glBegin(GL_TRIANGLES);
  for (long w=0;w<lwierszy;w++) 
    for (long k=0;k<lkolumn;k++) 
    {
      A = Wektor3(pocz_x + k*rozmiar_pola, mapa[w*2][k], pocz_z + w*rozmiar_pola);
      B = Wektor3(pocz_x + (k+0.5)*rozmiar_pola, mapa[w*2+1][k], pocz_z + (w+0.5)*rozmiar_pola);            
      C = Wektor3(pocz_x + (k+1)*rozmiar_pola, mapa[w*2][k+1], pocz_z + w*rozmiar_pola); 
      D = Wektor3(pocz_x + k*rozmiar_pola, mapa[(w+1)*2][k], pocz_z + (w+1)*rozmiar_pola);       
      E = Wektor3(pocz_x + (k+1)*rozmiar_pola, mapa[(w+1)*2][k+1], pocz_z + (w+1)*rozmiar_pola); 
      // tworz� tr�jk�t ABC w g�rnej cz�ci kwadratu: 
      //  A o_________o C
      //    |.       .|
      //    |  .   .  | 
      //    |    o B  | 
      //    |  .   .  |
      //    |._______.|
      //  D o         o E

      Wektor3 AB = B-A;
      Wektor3 BC = C-B;
      N = (AB*BC).znorm();          
      glNormal3f( N.x, N.y, N.z);
      glVertex3f( A.x, A.y, A.z);
      glVertex3f( B.x, B.y, B.z);
      glVertex3f( C.x, C.y, C.z);
      d[w][k][ABC] = -(B^N);          // dodatkowo wyznaczam wyraz wolny z r�wnania plaszyzny tr�jk�ta
      Norm[w][k][ABC] = N;          // dodatkowo zapisuj� normaln� do p�aszczyzny tr�jk�ta
      // tr�jk�t ADB:
      Wektor3 AD = D-A;
      N = (AD*AB).znorm();          
      glNormal3f( N.x, N.y, N.z);
      glVertex3f( A.x, A.y, A.z);
      glVertex3f( D.x, D.y, D.z);
      glVertex3f( B.x, B.y, B.z);
      d[w][k][ADB] = -(B^N);       
      Norm[w][k][ADB] = N;
      // tr�jk�t BDE:
      Wektor3 BD = D-B;
      Wektor3 DE = E-D;
      N = (BD*DE).znorm();          
      glNormal3f( N.x, N.y, N.z);
      glVertex3f( B.x, B.y, B.z);
      glVertex3f( D.x, D.y, D.z);     
      glVertex3f( E.x, E.y, E.z);  
      d[w][k][BDE] = -(B^N);        
      Norm[w][k][BDE] = N;  
      // tr�jk�t CBE:
      Wektor3 CB = B-C;
      Wektor3 BE = E-B;
      N = (CB*BE).znorm();          
      glNormal3f( N.x, N.y, N.z);
      glVertex3f( C.x, C.y, C.z);
      glVertex3f( B.x, B.y, B.z);
      glVertex3f( E.x, E.y, E.z);      
      d[w][k][CBE] = -(B^N);        
      Norm[w][k][CBE] = N;
    }		
    glEnd();
    glEndList(); 

    /*glNewList(PowierzchniaTerenu,GL_COMPILE);
    glBegin(GL_POLYGON);
    glNormal3f( 0.0, 1.0, 0.0);
    glVertex3f( -100, 0, -300.0);
    glVertex3f( -100, 0, 100.0);
    glVertex3f( 100, 0, 100.0);
    glVertex3f( 100, 0, -300.0);
    glEnd();
    glEndList();*/

}



void Teren::Rysuj(int szczegolowosc)
{
  int sz = szczegolowosc;

  // rysowanie powierzchni terenu:
  glCallList(PowierzchniaTerenu);

  // rysowanie r�nych przedmiot�w (monet, budynk�w, drzew)
  GLUquadricObj *Qcyl = gluNewQuadric();
  GLUquadricObj *Qdisk = gluNewQuadric();
  GLUquadricObj *Qsph = gluNewQuadric();
  GLfloat GreySurface[] = { 0.7f, 0.7f, 0.7f, 0.3f};
  for (long i=0;i<liczba_przedmiotow;i++)
  {
    glPushMatrix();

    glTranslatef(p[i].wPol.x,p[i].wPol.y,p[i].wPol.z);
    //glRotatef(k.w*180.0/PI,k.x,k.y,k.z);
    //glScalef(dlugosc,wysokosc,szerokosc);
	//if (p[i].typ != MONETA) { continue;  }

    switch (p[i].typ)
    {
    case MONETA:
      //gluCylinder(Qcyl,promien1,promien2,wysokosc,10,20);
      if (p[i].do_wziecia)
      {
        GLfloat Surface[] = { 2.0f, 2.0f, 1.0f, 1.0f};
        if (p[i].czy_zazn)
          glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GreySurface);
        else
          glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, Surface);
        glRotatef(90,1,0,0);
        p[i].srednica = powf(p[i].wartosc/100,0.4); float grubosc = 0.2*p[i].srednica;
        p[i].wPol.y = grubosc + Wysokosc(p[i].wPol.x,p[i].wPol.z);
        gluDisk(Qdisk,0,p[i].srednica,5*(1+sz),5*(1+sz));
        gluCylinder(Qcyl,p[i].srednica,p[i].srednica,grubosc,5*(1+sz),10*(1+sz)); 
        if (szczegolowosc == 1)
        {
          glRasterPos2f(0.30,1.20);
          glPrint("%d",p[i].wartosc); 
        }
      }
      break;
    case BECZKA:
      //gluCylinder(Qcyl,promien1,promien2,wysokosc,10,20);
      if (p[i].do_wziecia)
      {
        GLfloat Surface[] = { 0.50f, 0.45f, 0.0f, 1.0f};
        if (p[i].czy_zazn)
          glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GreySurface);
        else
          glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, Surface);
        glRotatef(90,1,0,0);
        p[i].srednica = powf((float)p[i].wartosc/50,0.4); float grubosc = 2*p[i].srednica;
        p[i].wPol.y = grubosc + Wysokosc(p[i].wPol.x,p[i].wPol.z);
        gluDisk(Qdisk,0,p[i].srednica,5*(1+sz),5*(1+sz));
        gluCylinder(Qcyl,p[i].srednica,p[i].srednica,grubosc,5*(1+sz),10*(1+sz)); 
        if (szczegolowosc == 1)
        {
          glRasterPos2f(0.30,1.20);
          glPrint("%d",p[i].wartosc); 
        }
      }
      break;
    case DRZEWO:
      switch (p[i].podtyp)
      {
      case TOPOLA:  
        {     
          GLfloat Surface[] = { 0.5f, 0.5f, 0.0f, 1.0f};
          if (p[i].czy_zazn)
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GreySurface);
          else
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, Surface);
          glPushMatrix();
          glRotatef(90,1,0,0);
          p[i].srednica = 0.8; float wysokosc = p[i].wartosc;
          p[i].wPol.y = wysokosc + Wysokosc(p[i].wPol.x,p[i].wPol.z);
          gluCylinder(Qcyl,p[i].srednica/2,p[i].srednica,wysokosc,5*(1+sz),10*(1+sz)); 
          glPopMatrix();
          GLfloat Surface2[] = { 0.0f, 0.9f, 0.0f, 1.0f};
          if (p[i].czy_zazn)
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GreySurface);
          else
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, Surface2);
          //glTranslatef(0,wysokosc,0);
          glScalef(1,2,1);
          gluSphere(Qsph,3,5*(1+3*sz),5*(1+3*sz));
          break;
        }	
      case SWIERK:	
        {
          GLfloat Surface[] = { 0.65f, 0.3f, 0.0f, 1.0f};
          if (p[i].czy_zazn)
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GreySurface);
          else
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, Surface);
          glPushMatrix();
          glRotatef(90,1,0,0);
          float wysokosc = p[i].wartosc;
          p[i].srednica = wysokosc/10;
          float promien = p[i].srednica/2;
          p[i].wPol.y = wysokosc + Wysokosc(p[i].wPol.x,p[i].wPol.z);
          gluCylinder(Qcyl,promien,promien*2,wysokosc,5*(1+sz),10*(1+sz)); 

          GLfloat Surface2[] = { 0.0f, 0.70f, 0.2f, 1.0f};
          if (p[i].czy_zazn)
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GreySurface);
          else
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, Surface2);
          glTranslatef(0,0,wysokosc*2/4);
          gluCylinder(Qcyl,promien*2,promien*8,wysokosc/4,5*(1+sz),10*(1+sz)); 
          glTranslatef(0,0,-wysokosc*1/4);
          gluCylinder(Qcyl,promien*2,promien*6,wysokosc/4,5*(1+sz),10*(1+sz)); 
          glTranslatef(0,0,-wysokosc*1/3);
          gluCylinder(Qcyl,0,promien*4,wysokosc/3,5*(1+sz),10*(1+sz)); 
          glPopMatrix();
          break;
        }
      case BAOBAB:
        {
          GLfloat Surface[] = { 0.5f, 0.9f, 0.2f, 1.0f};
          if (p[i].czy_zazn)
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GreySurface);
          else
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, Surface);

          int liczba = 10;
          glPushMatrix();
          glRotatef(90,1,0,0);
          float wysokosc = p[i].wartosc;
          glTranslated(0,wysokosc,0);
          gluCylinder(Qcyl,p[i].srednica/2,p[i].srednica,wysokosc*1.1,liczba,liczba*2); 
          glPopMatrix();
          GLfloat Surface2[] = { 0.0f, 0.9f, 0.2f, 1.0f};

          if (p[i].czy_zazn)
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GreySurface);
          else
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, Surface2);
          glPushMatrix();
          float s = 2 + wysokosc/20;
          glScalef(s,s/2,s);
          gluSphere(Qsph,3,liczba*2,liczba*2);
          glPopMatrix();
          glTranslatef(0,-s/1.5,0);
          glScalef(s*2.2,s/2,s*2.2);
          gluSphere(Qsph,3,liczba,liczba);

          break;
        }
      case FANTAZJA:
        {
          GLfloat KolorPnia[] = { 0.65f, 0.3f, 0.0f, 1.0f};                    // kolor pnia i ga��zi
          if (p[i].czy_zazn)
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GreySurface);
          else
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, KolorPnia);

          int liczba = 10;
          glPushMatrix();
          glRotatef(90,1,0,0);
          float wysokosc = p[i].wartosc, srednica_podst = p[i].srednica,
            srednica_wierzch = p[i].srednica/2;
          glTranslated(0,wysokosc,0);
          gluCylinder(Qcyl,srednica_wierzch,srednica_podst,wysokosc*1.1,liczba,liczba*2);  // rysowanie pnia
          glPopMatrix();
          glPushMatrix();
          GLfloat KolorLisci[] = { 0.5f, 0.65f, 0.2f, 1.0f};                    // kolor li�ci
          if (p[i].czy_zazn)
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GreySurface);
          else
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, KolorLisci);
          //glTranslatef(0,wysokosc,0);
          float s = 0.5 + wysokosc/15;    // �rednica listowia      
          glScalef(s,s,s);
          gluSphere(Qsph,3,liczba*2,liczba*2);        // rysowanie p�ku li�ci na wierzcho�ku
          glPopMatrix();

          int liczba_gal = (int)(wysokosc/3.5) + (int)(wysokosc*10) % 3;             // liczba ga��zi
          float prop_podz = 0.25 + (float)((int)(wysokosc*13) % 10)/100;           // proporcja wysoko�ci na kt�rej znajduje si� ga��� od poprzedniej galezi lub gruntu
          float prop = 1, wys = 0;        

          for (int j=0;j<liczba_gal;j++)
          {
            glPushMatrix();          
            prop *= prop_podz;                      
            wys += (wysokosc-wys)*prop_podz;              // wysoko�� na kt�rej znajduje si� i-ta ga��� od poziomu gruntu

            float kat = 3.14*2*(float)((int)(wys*107) % 10)/10;    // kat w/g pnia drzewa
            float sredn = (srednica_podst - wys/wysokosc*(srednica_podst-srednica_wierzch))/2;
            float dlug = sredn*2 + sqrt(wysokosc-wys);
            if (p[i].czy_zazn)
              glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GreySurface);
            else
              glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, KolorPnia);

            glTranslatef(0,wys-wysokosc,0);
            glRotatef(kat*180/3.14,0,1,0);
            gluCylinder(Qcyl,sredn,sredn/2,dlug,liczba,liczba);
            if (p[i].czy_zazn)
              glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, GreySurface);
            else
              glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, KolorLisci);

            float ss = s*sredn/srednica_podst*1.5;
            glTranslatef(0,0,dlug+ss*2);
            glScalef(ss,ss,ss);
            gluSphere(Qsph,3,liczba,liczba);        // rysowanie p�ku li�ci na wierzcho�ku

            glPopMatrix();
          }

          break;
        }

      }	
    }

    glPopMatrix();
  }
  gluDeleteQuadric(Qcyl);gluDeleteQuadric(Qdisk);gluDeleteQuadric(Qsph);
  //glCallList(Floor);                   
}

